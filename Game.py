# coding:utf-8
import time
import random
import erajs.api as a
import logic.main as m
import logic.editor as editor
import logic.游戏 as yx
import logic.世界 as sj
import logic.人物 as rw
# 注意！不推荐在此处定义变量，而应在 a.init() 之后将变量定义在 a.data['db'] 中，以获得数据持久性支持。

version = '0.1.0'


def cover():
    a.title('EraLife v{} with Era.js v{}'.format(version, a.version))
    a.page()
    a.h('EraLife v{}'.format(version))
    a.t('with Era.js v{}'.format(a.version))
    a.t()
    a.t()
    a.b('开始游戏', a.goto, start_new_game)
    a.t()
    a.b('加载游戏', a.goto, load_game)
    a.t()
    a.b('游戏设置', None, disabled=True)
    a.t()
    a.b('游戏编辑器', a.goto, editor.cover)
    a.t()
    a.b('退出游戏', a.exit)


def start_new_game():
    """开始新游戏"""
    a.page()
    a.data['gm'].new_game()
    a.page()
    a.h('请选择游戏模式')
    a.t()
    a.b('默认游戏模式', a.goto, default_mode)
    a.t()
    a.b('自定义游戏模式', None, disabled=True)
    a.t()
    a.b('返回', a.back)


def default_mode():
    """应用默认游戏模式"""
    a.page()
    a.t('已选择默认游戏模式！', True)
    # TODO(Miswanting): 显示游戏默认设置内容
    # 初始化世界
    a.page()
    a.h('请选择玩家创建方式')
    a.t()
    a.b('默认玩家角色', a.goto, default_player)
    a.t()
    a.b('自定义玩家角色', a.goto, custom_player)
    a.t()
    a.b('返回', a.back)


def default_player():
    def add_person(person):
        a.data['pm'].add_person(player)
        a.data['db']['player'] = player.data['hash']
        # 进入出生地
        a.data['wm'].add_to_birth_place(player.data['hash'])
        a.page()
        a.t('已设定！', True)
        a.goto(ask_intro)
    player = a.data['pm'].new_null_person()
    player.data['name'] = '你'
    player.data['系统称呼'] = '你'
    player.data['money'] = 100
    player.update()
    a.page()
    a.h('默认玩家角色')
    a.t()
    a.data['pm'].show_person_profile(player)
    a.t()
    a.t('确定吗？')
    a.t()
    a.b('是', add_person, player)
    a.t('/')
    a.b('否', a.back)


def custom_player():
    def gui_set_name():
        a.page()
        a.h('请设置人物名称&称呼')
        a.t()
        a.t('默认名称:【{}】　自定义:'.format(player.data['name']))
        a.input(set_name)
        a.t()
        a.t('默认系统称呼:【{}】　自定义:'.format(player.data['系统称呼']))
        a.input(set_sys_name)
        a.t()
        a.t('默认自称:【{}】　自定义:'.format(player.data['自称']))
        a.input(set_self_name)
        a.t()
        a.t('默认尊称:【{}】　自定义:'.format(player.data['尊称']))
        a.input(set_resp_name)
        a.t()
        a.t('默认蔑称:【{}】　自定义:'.format(player.data['蔑称']))
        a.input(set_diss_name)
        a.t()
        a.b('确定', set_attr)

    def set_name(data):
        player.data['name'] = data

    def set_sys_name(data):
        player.data['系统称呼'] = data

    def set_self_name(data):
        player.data['自称'] = data

    def set_resp_name(data):
        player.data['尊称'] = data

    def set_diss_name(data):
        player.data['蔑称'] = data

    def set_attr():
        a.page()
        a.h('请设置人物属性')
        a.t()
        a.t('性别：')
        a.radio(['男', '女'], func=set_gender)
        a.t()
        a.t('身高：')
        a.radio(['矮个', '普通', '高个'], 1, set_height)
        a.t()
        a.t('体型：')
        a.radio(['瘦弱', '苗条', '普通', '丰满', '肥胖'], 2, set_build)
        a.t()
        a.t('体质：')
        a.radio(['虚弱', '普通', '强壮'], 1, set_strong)
        a.t()
        a.t()
        a.b('确定', confirm_player)
        a.b('返回', a.back)

    def set_gender(value):
        player.data['gender'] = value

    def set_height(value):
        if not value == '普通':
            player.data['tag'].append(value)

    def set_build(value):
        if not value == '普通':
            player.data['tag'].append(value)

    def set_strong(value):
        if not value == '普通':
            player.data['tag'].append(value)

    def confirm_player():
        a.page()
        player.data['money'] = 100
        player.update()
        player.reset()
        a.data['pm'].show_person_profile(player)
        a.t()
        a.t('确定吗？')
        a.t()
        a.b('是', add_person)
        a.t('/')
        a.b('否', a.back)

    def add_person():
        player.save()
        a.data['pm'].add_person(player)
        a.data['db']['player'] = player.data['hash']
        a.data['wm'].add_to_birth_place(player.data['hash'])
        a.page()
        a.t('已设定！', True)
        a.goto(ask_intro)

    player = a.data['pm'].new_null_person()
    player.data['name'] = '亚当'
    player.data['系统称呼'] = '你'
    player.data['自称'] = '我'
    player.data['尊称'] = '你'
    player.data['蔑称'] = '你'
    a.goto(gui_set_name)


def ask_intro():
    a.page()
    a.t('是否欣赏开场剧情？')
    a.t()
    a.b('是', a.goto, intro)
    a.t('/')
    a.b('否', a.goto, short_intro)


def intro():
    a.page()
    for each in a.data['data.others']['opening_long']:
        last = len(each)
        for every in each:
            a.t(every)
            time.sleep(0.5/last)
            last -= 1
        a.t()
    a.t(' ', True)
    a.goto(short_intro)


def short_intro():
    a.page()
    for each in a.data['data.others']['opening_short']:
        a.t(each, True)
    a.clear_gui()
    a.goto(loop)


def load_for_load():
    a.data['pm'].load_all_person()
    a.data['wm'].load_all_world()
    a.clear_gui()
    a.goto(loop)


def loop():
    a.page()
    a.t(random.choice(a.data['data.others']['tips']))
    a.t()
    a.divider()
    a.t()
    m.show_main_panel()
    a.t()
    place_type = a.data['wm'].find_person(a.data['db']['player'])[2]['type']
    func = a.data['data.地点'][place_type]['功能']
    for each in func.keys():
        if each == '郊区':
            a.b('郊区探险', a.goto, m.explore)
        elif each == '市场':
            a.b('市场', a.goto, m.market)
        elif each == '酒馆':
            a.b('酒馆', m.bar)
    a.t()
    a.b('人物管理', a.goto, m.person_list)
    player = a.data['pm'].get_player()
    has_slave = False
    for person_hash in player.data['关系'].keys():
        if '被囚禁者' in player.data['关系'][person_hash]['关系']:
            has_slave = True
            break
    if has_slave:
        a.b('奴隶管理', a.goto, m.ui_slave_list)
    else:
        a.b('奴隶管理', None, disabled=True)
    a.b('装备管理', a.goto, m.equipment)
    a.b('物品管理', None, disabled=True)
    a.b('百科全书', a.goto, a.data['wiki'].ui_wiki)
    a.b('休息', rest)
    a.t()
    a.b('保存游戏', a.goto, save_game)
    a.b('读取游戏', a.goto, load_game)
    a.b('游戏设置', None, disabled=True)
    a.b('结局', None, disabled=True)
    a.b('结束游戏', a.exit)


def rest():
    a.tick()
    a.page()
    m.rest()
    a.t('经过了半天。', True)
    a.repeat()


def save_game():
    a.page()
    a.h('保存游戏')
    a.t()
    a.show_save_to_save()
    a.b('返回', a.back)


def quit_game(a):
    pass


def load_game():
    a.page()
    a.h('读取游戏')
    a.t()
    a.show_save_to_load(load_for_load)
    a.b('返回', a.back)


if __name__ == "__main__":
    try:
        a.init()
        # 初始化游戏
        a.data['gm'] = yx.GameManager()
        a.goto(cover)
    except Exception as e:
        a.critical('{}'.format(e))
