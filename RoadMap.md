# Roadmap

- [ ] 游戏本体：EraLife

  - [ ] 新建游戏
    - [x] 使用默认世界
    - [x] 使用默认角色
    - [x] 使用自定义角色
      - [x] 自定义姓名
      - [x] 自定义性别
      - [x] 自定义身高
      - [x] 自定义体重
      - [x] 自定义强壮
  - [x] 保存游戏
  - [x] 加载游戏
  - [ ] 游戏机制
    - [x] 游戏初始化
    - [ ] 互动机制
      - [ ] 互动初始化
      - [ ] 战斗机制
      - [ ] ＴＪ 机制
      - [ ] 社交机制
      - [ ] 结算机制
      - [ ] 口上机制
      - [ ] 动作描述机制
      - [ ] 属性/技能加点
    - [x] 人物档案机制
      - [x] 基本信息
      - [x] 属性
      - [x] 装备
      - [ ] 外观
      - [ ] 技能
    - [x] 物品机制
      - [ ] 购买物品
        - [ ] 市场
      - [ ] 装备物品
      - [x] 查看物品信息
        - [x] 武器
        - [x] 防具
    - [x] 酒馆
      - [ ] 管理员
      - [ ] 顾客
      - [ ] 直接互动
      - [ ] 间接互动
    - [x] 休息
    - [ ] EraLife 百科
      - [ ] 分页显示
  - [ ] 游戏设置
    - [ ] DLC 管理
    - [ ] MOD 管理
    - [ ] 游戏设置
      - [ ] 是否总是对主角使用系统称呼

  - [ ] 退出游戏

- [ ] 游戏编辑器：EraEditor

  - [ ] 元数据编辑器
  - [ ] 游戏数据编辑器
  - [ ] 数据文件编辑器
  - [ ] 存档编辑器
  - [ ] 编辑器设置

- [ ] 游戏引擎：Era.js

  - [ ] API
    - [ ] 控件
    - [ ] 界面逻辑
    - [ ] 数据
  - [ ] 插件
    - [ ] EraAchievement：成就系统
    - [ ] EraAct：行为模拟系统
    - [ ] EraAI：人工智能系统（非神经网络）
    - [ ] EraAvantar：人物头像系统
    - [ ] EraBattle：真实战斗系统
    - [ ] EraCareer：职业生涯系统
    - [ ] EraChaos：混沌核心系统
    - [ ] EraConsole：游戏内命令行系统
    - [ ] EraData：先进数据管理系统
    - [ ] EraDebug：运行时Debug系统
    - [ ] EraDynamic：（基于回合的）动态世界系统
    - [ ] EraEmotion：情感模拟系统
    - [ ] EraERB：ERB兼容系统
    - [ ] EraEstate：不动产系统
    - [ ] EraEvent：事件管理系统
    - [ ] EraFestival：节日系统
    - [ ] EraGuanXi：复杂人物关系系统
    - [ ] EraHistory：历史管理系统
    - [ ] EraIndoor：室内系统
    - [ ] EraItem：物品管理系统
    - [ ] EraKojo：特殊人物口上系统
    - [ ] EraKojoGen：人物口上生成系统
    - [ ] EraLuck：幸运管理系统
    - [ ] EraMap：地图系统
    - [ ] EraMarket：市场系统
    - [ ] EraNative：原生功能管理系统
    - [ ] EraNet：联机系统
    - [ ] EraRealtime：实时系统
    - [ ] EraSElement：七元素系统
    - [ ] EraTern：先进回合系统
    - [ ] EraTextTemp：文本模板系统
    - [x] EraTime：时间管理系统
    - [ ] EraUI：先进游戏界面描述系统

- [ ] 游戏MOD

  - [ ] MOD：