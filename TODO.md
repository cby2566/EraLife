# 开发计划

[TOC]

## 本体

本体是指除DLC、项目库、引擎之外的其他代码。

### v0.1

## DLC

### `DLC 0: Core: 核心`

### `DLC 1: Ordinary Life: 日常生活`

#### 玩法

用`自己`的`存款` `买一套装备`，`穿在身上`，`出门探险`，经过`战斗`与`寻宝`，`获取经验与金钱`。

### `DLC 2: Simple Love: 简单恋爱`

#### 玩法

通过偶遇的方式认识其他人，并通过各种交流方式来提升友情、爱情、亲情。

### `DLC 3: Sweet Home: 居家生活`

#### 玩法

通过购买房产，扩建房屋，出售房屋等方式，为自己未来的生活提供无限可能，另外，你可以邀请你亲密的恋人参观你的卧室。

### `DLC 4: Deep Love: 深爱`

### `DLC 4: Special Love: 特别的爱`

### `DLC 5: Special Love: 特别之爱`

### `DLC 6: Special Love: 特别之爱`

### `DLC 7: Special Love: 特别之爱`

### `DLC 8: Special Love: 特别之爱`

### `DLC 9: Special Love: 特别之爱`

## 项目库

### v0.1

## 文字库

### v0.1