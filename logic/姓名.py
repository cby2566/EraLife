import random
import erajs.api as a


# def get_random_name(gender, region='外'):
#     name_db = a.data['data.姓名库']
#     gender_data = a.data['data.others']['性别']
#     if gender in gender_data.keys():
#         name_space = gender_data[gender]['使用姓名库']
#     if region == '中':
#         name = random.choice(name_db['中文']['姓'])
#         if name_space == '男':
#             name += random.choice(name_db['中文']['男名'])
#         elif name_space == '女':
#             name += random.choice(name_db['外文']['女名'])
#         return name
#     elif region == '外':
#         if name_space == '男':
#             name = random.choice(name_db['外文']['男名'])
#         elif name_space == '女':
#             name = random.choice(name_db['外文']['女名'])
#         return name
def get_random_name(gender=''):
    name_db = a.data['data.英文名库']
    gender_data = a.data['data.others']['性别']
    candidate_list = []
    if gender == '':
        candidate_list.extend(name_db)
    else:
        if gender in gender_data.keys():
            name_space = gender_data[gender]['使用姓名库']
        if name_space == '男':
            for each in name_db:
                if each[4] in ['男性', '中性']:
                    candidate_list.append(each)
        elif name_space == '女':
            for each in name_db:
                if each[4] in ['女性', '中性']:
                    candidate_list.append(each)
    name = random.choice(candidate_list)
    return name[1]
