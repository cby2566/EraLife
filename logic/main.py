import random
import erajs.api as a


def show_person_profile(person):
    pm = a.data['pm']
    a.t('{}　{}　Lv:{}　EXP:({}/{})　金钱:￥{}'.format(person['name'],
                                                person['gender'],
                                                person['level'],
                                                person['exp'],
                                                person['exp_limit'],
                                                person['money'],))
    a.t()
    a.t('体力:({}/{})'.format(person['体力'],
                            person['体力上限']))
    a.t('　耐力:({}/{})'.format(person['耐力'],
                             person['耐力上限']))
    a.t('　精力:({}/{})'.format(person['精力'],
                             person['精力上限']))
    a.t()
    a.t('力量:{}　敏捷:{}　智力:{}　未使用属性点:{}'.format(person['力量'],
                                             person['敏捷'],
                                             person['智力'],
                                             person['属性点']))
    a.t()
    a.t('武器:[{}]'.format(person['武器']))


def show_person_preview(person):
    a.t('■{}　{}■'.format(person['name'],
                         person['gender']))


def show_main_panel():
    a.t(a.get_full_time())
    player = a.data['pm'].get_person(a.data['db']['player'])
    a.t('　金币:￥{}'.format(player.data['money']))
    a.t()
    a.t('{} {} Lv:{} EXP:({}/{})'.format(player.data['name'],
                                         player.data['gender'],
                                         player.data['level'],
                                         player.data['exp'],
                                         player.data['exp_limit']))
    a.progress(player.data['exp'], player.data['exp_limit'])
    a.t()
    a.t('体力:({}/{})'.format(player.data['体力'],
                            player.data['体力上限']))
    a.progress(player.data['体力'],
               player.data['体力上限'])
    a.t('　耐力:({}/{})'.format(player.data['耐力'],
                             player.data['耐力上限']))
    a.progress(player.data['耐力'],
               player.data['耐力上限'])
    a.t('　精力:({}/{})'.format(player.data['精力'],
                             player.data['精力上限']))
    a.progress(player.data['精力'],
               player.data['精力上限'])


def person_list():
    a.page()
    a.h('人物列表')
    a.t()
    person = a.data['pm'].get_person(a.data['db']['player'])
    a.b('{}　{} －－－－－－－－'.format(person.data['name'],
                                person.data['gender']),
        a.goto, a.data['pm'].show_profile, person)
    a.t()
    a.b('返回', a.back)


def profile(person):
    a.page()
    a.h('人物档案')
    a.t()
    a.t('{}　{}　Lv:{}　EXP:({}/{})'.format(person.data['name'],
                                         person.data['gender'],
                                         person.data['level'],
                                         person.data['exp'],
                                         person.data['exp_limit']))
    a.progress(person.data['exp'], person.data['exp_limit'])
    a.t()
    a.t('体力:({}/{})'.format(person.data['体力'],
                            person.data['体力上限']))
    a.progress(person.data['体力'],
               person.data['体力上限'])
    a.t('　耐力:({}/{})'.format(person.data['耐力'],
                             person.data['耐力上限']))
    a.progress(person.data['耐力'],
               person.data['耐力上限'])
    a.t('　精力:({}/{})'.format(person.data['精力'],
                             person.data['精力上限']))
    a.progress(person.data['精力'],
               person.data['精力上限'])
    a.t()
    a.t('力量:{}　敏捷:{}　智力:{}　'.format(person.data['力量'],
                                    person.data['敏捷'],
                                    person.data['智力']))
    attr_point = person.data['属性点']
    if person.data['hash'] == a.data['db']['player']:
        if attr_point > 0:
            a.b('属性点:{}'.format(attr_point), a.goto, add_attr_point, person)
        else:
            a.b('属性点:{}'.format(attr_point), None, disabled=True)
        a.b('武器:[{}]'.format(person.data['武器']), a.goto, equipment)
        a.b('防具管理', None, disabled=True)
        a.b('服装管理', None, disabled=True)
    a.t()
    a.t('体型:{}　身高：{}cm　体重:{}kg　三围:{}/{}/{}')
    a.t()
    a.t('属性:')
    a.t()
    a.t('经验:')
    a.t()
    a.t('异常经验:')
    a.t()
    a.t('参数:')
    a.t()
    a.t('刻印:')
    a.t()
    a.t('素质:')
    a.t()
    a.t('性别:')
    a.t()
    a.t('种族:')
    a.t()
    a.t('职业:')
    a.t()
    a.t('性格:')
    a.t()
    a.t('体质:')
    a.t()
    a.t('技能:')
    a.t()
    a.t('魔法:')
    a.t()
    a.t('后天:')
    a.t()
    a.t('弱点:')
    a.t()
    a.t('助手职责:')
    a.t()
    a.t('指示:')
    a.t()
    a.t('服装:')
    a.t()
    a.t('关系:')
    a.t()
    a.t('作品:')
    a.t()
    a.t('榨精排行:')
    a.t()
    a.b('返回', a.back)


def add_attr_point(person):
    def add_attr(x):
        if person.data['属性点'] > 0:
            person.data['属性点'] -= 1
            person.data[x] += 1
            a.repeat()
        else:
            a.page()
            a.t('没有未使用的属性点！', True)
            a.repeat()

    a.page()
    a.h('待分配属性点：{}'.format(person.data['属性点']))
    a.t()
    a.t('力量:{}　'.format(person.data['力量']))
    a.b('+', add_attr, '力量')
    a.t()
    a.t('敏捷:{}　'.format(person.data['敏捷']))
    a.b('+', add_attr, '敏捷')
    a.t()
    a.t('智力:{}　'.format(person.data['智力']))
    a.b('+', add_attr, '智力')
    a.t()
    a.b('返回', a.back)


def rest():
    # 全局人物回复状态
    a.data['pm'].load_all_person()
    for person_hash in a.data['db']['person'].keys():
        person = a.data['pm'].get_person(person_hash)
        if not person == None:
            person.data['体力'] += person.data['体力上限']*0.7
            person.data['耐力'] += person.data['耐力上限']*0.7
            person.data['精力'] += person.data['精力上限']*0.7
            person.update()
    # 酒馆人物刷新
    refresh_bar()


def show_main_gui():
    a.t('时间')
    a.t('金钱')
    a.t()
    a.divider()
    a.t()
    a.t('我的姓名/性别')
    a.t('我的状态（体力等）')
    a.t()
    a.t('我的装束')
    a.t()
    a.divider()
    a.t()
    a.t('各个行动选项')


# def explore():
#     if 'battle_end' in a.data.keys() and a.data['battle_end']:
#         a.data['battle_end'] = False
#         if a.data['pm'].get_player().data['体力'] <= 0:
#             a.data['pm'].get_player().data['体力'] = 1
#         a.back()
#     else:
#         a.page()
#         battle = a.data['bm'].new_battle()
#         battle.add_person(a.data['pm'].get_person(
#             a.data['db']['player']), 'company')
#         for i in range(random.randint(1, 1)):
#             enemy = a.data['pm'].new_random_person(
#                 a.data['pm'].get_person(a.data['db']['player']).data['level'], 1)
#             enemy.update()
#             a.t('一个自称为{}的{}{}出现了！'.format(
#                 enemy.data['name'], enemy.data['gender'], enemy.data['种族']))
#             battle.add_person(enemy, 'enemy')
#         battle.start_battle()

def explore():
    # explore 初始化
    a.page()
    # 探险多少次？
    meets = random.randint(2, 5)
    # 每次遇到哪些人？
    enemy_hash_list = random.choices(
        a.data['wm'].find_player()[2]['person'], k=1)
    for enemy_hash in enemy_hash_list:
        enemy = a.data['pm'].get_person(enemy_hash)
        a.t('一个自称为{}的{}{}出现了！'.format(
            enemy.data['name'], enemy.data['gender'], enemy.data['种族']), True)
        a.t()
        player = a.data['pm'].get_player()
        # 新建交互
        interaction = a.data['im'].new_interaction()
        interaction.add_person(player)
        interaction.add_person(enemy, 1)
    # 开打还是溜？
    # 开打就进入互动
    interaction.set_scene('战斗')
    a.clear_gui(1)
    interaction.start()


def market():
    def shop(t):
        def buy_confirm(item):
            a.page()
            a.t('您确定要购买{}吗？'.format(item))
            a.t()
            a.b('是', buy, item)
            a.t('/')
            a.b('否', a.back)

        def buy(item):
            player = a.data['pm'].get_person(a.data['db']['player'])
            if player.data['money'] >= a.data['data.物品'][item]['价格']:
                player.data['money'] -= a.data['data.物品'][item]['价格']
                player.data['物品'].append(item)
                a.page()
                a.t('你购买了{}！'.format(item), True)
                a.back()
            else:
                a.page()
                a.t('你没有足够的金钱！', True)
                a.back()

        a.page()
        a.h('{}'.format(t))
        a.t()
        for each in a.data['data.物品'].keys():
            if a.data['data.物品'][each]['可购买']:
                if a.data['data.物品'][each]['商店'] == t:
                    a.b('[{}]￥{}'.format(
                        each, a.data['data.物品'][each]['价格']), a.goto, buy_confirm, each)
        a.t()
        a.b('返回', a.back)

    a.page()
    a.t('你来到了市场，市场里人群熙熙攘攘。', True)
    a.page()
    a.h('市场')
    a.t()
    for each in a.data['data.others']['市场'].keys():
        a.b('{}'.format(each), a.goto, shop, each)
        a.t()
    a.b('返回', a.back)


def refresh_bar():
    seats = []
    酒馆桌子数目 = a.data['wm'].find_player()[2]['area']['酒馆']['酒馆桌子数目']
    酒馆上座率 = a.data['wm'].find_player()[2]['area']['酒馆']['酒馆上座率']
    if not '去过酒馆' in a.data['db'].keys():
        for i in range(酒馆桌子数目+1):
            table = ['', '', '', '']
            seats.append(table)
    else:
        person_list = a.data['wm'].find_player()[2]['person'][0:]
        person_list = random.sample(person_list, len(person_list))
        for i in range(酒馆桌子数目+1):
            table = ['', '', '', '']
            if random.random() < 酒馆上座率:
                for j in range(4):
                    if random.choice([True, False]):
                        if len(person_list) >= 0:
                            person_hash = person_list.pop(0)
                            if not a.data['pm'].get_player().data['hash'] == person_hash:
                                if not a.data['wm'].find_player()[2]['area']['酒馆']['admin'] == person_hash:
                                    table[j] = person_hash
            seats.append(table)
    a.data['wm'].find_player()[2]['area']['酒馆']['seats'] = seats


def bar():
    def go_bar():
        seats = a.data['wm'].find_player()[2]['area']['酒馆']['seats']
        a.page()
        a.h('酒馆')
        a.t()
        a.t('老板:')
        if boss.data['gender'] == '男':
            color = 'blue'
        elif boss.data['gender'] == '女':
            color = 'red'
        a.b('{}'.format(boss.data['name']), chat_with_boss, boss, color=color)
        a.t('【吧台】')
        for j, person in enumerate(seats[0]):
            if not person == '' and not vacuum:
                show_seat(person)
            else:
                a.b('空', sit_in_desk, 0, j)
        a.t()
        a.t()
        for i, table in enumerate(seats[1:]):
            a.t('【{}号桌】'.format(i+1))
            for j, person in enumerate(table):
                if not person == '' and not vacuum:
                    show_seat(person)
                else:
                    a.b('空', sit_in_desk, i+1, j)
            if i % 2 == 0:
                a.t('　　　　')
            else:
                a.t()
        a.t()
        a.b('离开', a.back)

    def show_seat(person_hash):
        person = a.data['pm'].get_person(person_hash)
        if person.data['gender'] == '男':
            gender = '男性'
            color = 'blue'
        elif person.data['gender'] == '女':
            gender = '女性'
            color = 'red'
        tag = ['瘦弱', '苗条', '丰满', '肥胖']
        build = '普通'
        for each in tag:
            if each in person.data['tag']:
                build = each
        popup = '一位身材{}的{}{}'.format(build, gender, person.data['种族'])
        a.b('{}'.format(person.data['name']),
            chat_with_person, person, color=color, popup=popup)

    def chat_with_person(person):
        a.page()
        if person.data['gender'] == '男':
            gender = '男性'
        elif person.data['gender'] == '女':
            gender = '女性'
        a.t('你来到这位{}{}面前，{}抬起眼睛看了你一眼。'.format(gender,
                                              person.data['种族'],
                                              person.data['第三人称']))
        interact_with_person(person)

    def chat_with_boss(boss):
        a.page()
        if boss.data['gender'] == '男':
            gender = '男性'
        elif boss.data['gender'] == '女':
            gender = '女性'
        a.t('你来到酒馆老板面前，{}抬起眼睛看了你一眼。'.format(boss.data['第三人称']))
        interact_with_person(boss)

    def interact_with_person(person):
        interaction = a.data['im'].new_interaction()
        player = a.data['pm'].get_player()
        interaction.add_person(player)
        interaction.add_person(person, 1)
        interaction.set_scene('酒馆')
        interaction.start()

    def sit_in_desk(table, desk):
        pass
    vacuum = True
    tmp = a.data['wm'].find_player()
    boss_hash = tmp[2]['area']['酒馆']['admin']
    boss = a.data['pm'].get_person(boss_hash)
    boss.save()
    if not '去过酒馆' in a.data['db'].keys():
        refresh_bar()
        a.data['db']['去过酒馆'] = True
        a.page()

        if boss.data['gender'] == '男':
            gender = '男性'
        elif boss.data['gender'] == '女':
            gender = '女性'
        p = {
            'name': boss.data['name'],
            'gender': gender,
            '种族': boss.data['种族'],
        }
        for each in a.data['data.others']['bar_intro']:
            a.t(each.format(**p), True)
            a.t()
    else:
        vacuum = False
    a.goto(go_bar)


def equipment():
    def change_equipment(pos):
        a.page()
        a.t('{}修改为：'.format(pos))
        a.t()
        print(pm.get_player().data)
        for each in pm.get_player().data['物品']:
            print(a.data['data.物品'][each]['类型'], pos)
            if a.data['data.物品'][each]['类型'] == pos:
                a.b(each, confirm, pos, each)
        a.t()
        a.b('返回', a.back)

    def confirm(pos, item):
        a.page()
        a.t('确定要将{}修改为{}吗？'.format(pos, item))
        a.t()
        a.b('是', change, pos, item)
        a.t('/')
        a.b('否', a.back)

    def change(pos, item):
        a.page()
        pm.get_person(a.data['db']['player']).data[pos] = item
        a.t('你已将{}替换为{}。'.format(pos, item), True)
        print(pm.get_person(a.data['db']['player']).data[pos])
        a.back()

    a.page()
    a.h('装备管理')
    a.t()
    pm = a.data['pm']
    a.b('武器：[{}]'.format(pm.get_person(a.data['db']['player']).data['武器']),
        a.goto, change_equipment, '武器')
    a.t()
    a.b('返回', a.back)


def item():
    a.page()
    a.h('物品管理')
    a.t()
    for each in a.data['db']['player']['背包']:
        a.b(each, None)
    a.t()
    a.b('返回', a.back)


# def get_null_person():
#     person = {
#         # 系统
#         'name': 'N/A',
#         'hash': a.new_hash(),
#         'tag': [],
#         # 称呼
#         '系统称呼': 'N/A',
#         '自称': '我',
#         '尊称': '您',
#         '蔑称': '你',
#         '第三人称': '他',
#         # 关系
#         '父亲': '',
#         '母亲': '',
#         '子女': [],
#         '恋人': [],
#         '配偶': '',
#         '关系': {},
#         # 灵魂
#         'level': 1,
#         'exp': 0,
#         'exp_limit': 100,
#         'buff': [],
#         'money': 0,
#         '职业': '',
#         '智力': 1,
#         '性格': [],
#         '思想': [],
#         '技能': [],
#         '履历': [],
#         '刻印': [],
#         '属性点': 0,
#         # 身体
#         'gender': '男',
#         'age': 0,
#         '生日': 0,
#         '种族': '人类',
#         '身高': 150,
#         '体重': 50,
#         '胸围': 50,
#         '腰围': 50,
#         '臀围': 50,
#         '力量': 1,
#         '敏捷': 1,
#         '体力': 100,
#         '耐力': 100,
#         '精力': 100,
#         '体力上限': 100,
#         '耐力上限': 100,
#         '精力上限': 100,
#         '状态': {
#             '快': {
#                 'c': 0,
#                 'v': 0,
#                 'a': 0,
#                 'b': 0,
#                 'm': 0,
#                 'u': 0,
#             },
#             '润': {
#                 'v': 0,
#                 'a': 0,
#                 'u': 0,
#             },
#         },
#         '素质': {
#             '感': {
#                 'c': 0,
#                 'v': 0,
#                 'a': 0,
#                 'b': 0,
#                 'm': 0,
#                 'u': 0,
#             },
#             '名': {
#                 'v': 0,
#                 'a': 0,
#                 'b': 0,
#                 'm': 0,
#                 'u': 0,
#             },
#             '倾向': {
#                 's': 0,
#                 'm': 0,
#             },
#             '技术': {
#                 'm': 0,
#                 'b': 0,
#                 'h': 0,
#                 'v': 0,
#                 'a': 0,
#                 'f': 0,
#             },
#         },
#         '特点': [],
#         # 服装
#         '内衣': '',
#         '内裤': '',
#         '衣服': '',
#         '裤子': '',
#         '袜子': '',
#         '鞋子': '',
#         '帽子': '',
#         '面具': '',
#         '手套': '',
#         '外套': '',
#         '项链': '',
#         '手镯': '',
#         '戒指': '',
#         # 装备
#         '武器': '',
#         '头部': '',
#         '面部': '',
#         '颈部': '',
#         '上身': '',
#         '左手': '',
#         '右手': '',
#         '左指': '',
#         '右指': '',
#         '下身': '',
#         '脚部': '',
#         '背包': [],
#     }
#     return person


# def get_default_person():
#     person = get_null_person()
#     person['name'] = '你'
#     person['money'] = 100
#     update_person(person)
#     reset_person(person)
#     return person


# def get_random_person(level, up_to=2, gender=''):
#     person = get_null_person()
#     # 随机性别
#     if gender == '':
#         person.data['gender'] = random.choice(a.data['data.性别'])['name']
#     else:
#         person['gender'] = gender
#     # 随机姓名
#     if person['gender'] == '男':
#         person['name'] = random.choice(a.data['data.姓名库']['外文']['男名'])
#     elif person['gender'] == '女':
#         person['name'] = random.choice(a.data['data.姓名库']['外文']['女名'])
#         person['第三人称'] = '她'
#     person['种族'] = random.choice(a.data['data.种族'])['name']
#     # 随机种族
#     person['种族'] = random.choice(a.data['data.种族'])['name']
#     # 随机等级
#     person['level'] = random.randint(level, level+up_to)
#     # 随机力量/敏捷/智力
#     for i in range(person['level']-1):
#         tmp = random.choice(['力量', '敏捷', '智力'])
#         person[tmp] += 1
#     # 随机金钱
#     person['money'] = random.randint(
#         (person['level']-1)*10+1, (person['level']-1)*20+10)
#     # 随机身高
#     tmp = random.choice(a.data['data.others']['身高'])
#     if not tmp['name'] == '普通':
#         person['tag'].append(tmp['name'])
#     # 随机体型
#     tmp = random.choice(a.data['data.others']['体型'])
#     if not tmp['name'] == '普通':
#         person['tag'].append(tmp['name'])
#     # 随机体质
#     tmp = random.choice(list(a.data['data.others']['体质'].keys()))
#     if not tmp == '普通':
#         person['tag'].append(tmp)
#     update_person(person)
#     # 随机经验值
#     person['exp'] = random.randint(0, person['exp_limit'])
#     reset_person(person)
#     return person


# def update_person(person):
#     # 确定是否升级
#     person.data['exp_limit'] = person.data['level']*100
#     if person.data['exp'] >= person.data['exp_limit']:
#         a.t('{}升级了！'.format(person.data['name']))
#         person.data['exp'] -= person.data['exp_limit']
#         person.data['属性点'] += 1
#         person.data['level'] += 1
#         update_person(person)
#     # 计算三力上限
#     person.data['体力上限'] = (person.data['level']-1)*50 + \
#         (person.data['力量']-1)*50+100
#     person.data['耐力上限'] = (person.data['level']-1)*50 + \
#         (person.data['敏捷']-1)*50+100
#     person.data['精力上限'] = (person.data['level']-1)*50 + \
#         (person.data['智力']-1)*50+100
#     if '虚弱' in person['tag']:
#         rate = a.data['data.others']['体质']['虚弱']['rate']
#         person['体力上限'] *= rate
#         person['耐力上限'] *= rate
#         person['精力上限'] *= rate
#     if '强壮' in person['tag']:
#         rate = a.data['data.others']['体质']['强壮']['rate']
#         person['体力上限'] *= rate
#         person['耐力上限'] *= rate
#         person['精力上限'] *= rate
#     # 整数化三力上限
#     person['体力上限'] = int(person['体力上限'])
#     person['耐力上限'] = int(person['耐力上限'])
#     person['精力上限'] = int(person['精力上限'])
#     # 压制三力
#     if person['体力'] > person['体力上限']:
#         person['体力'] = person['体力上限']
#     if person['耐力'] > person['耐力上限']:
#         person['耐力'] = person['耐力上限']
#     if person['精力'] > person['精力上限']:
#         person['精力'] = person['精力上限']
#     # 整数化三力
#     person['体力'] = int(person['体力'])
#     person['耐力'] = int(person['耐力'])
#     person['精力'] = int(person['精力'])
#     # 修正空手
#     if person['武器'] == '':
#         person['武器'] = '空手'
#     return person


# def reset_person(person):
#     person['体力'] = person['体力上限']
#     person['耐力'] = person['耐力上限']
#     person['精力'] = person['精力上限']
#     return person


def ui_slave_list():
    def decide(slave):
        a.page()
        a.h('对{}采取的操作'.format(slave.data['name']))
        a.t()
        a.t()
        a.b('调教{}'.format(slave.data['第三人称']), tj, slave)
        a.b('看状态', None, disabled=True)
        a.b('将他加入多人互动', None, disabled=True)
        a.b('释放{}'.format(slave.data['第三人称']), None, disabled=True)
        a.b('出售{}'.format(slave.data['第三人称']), None, disabled=True)
        a.t()
        a.b('返回', a.back)

    def tj(slave):
        a.page()
        player = a.data['pm'].get_player()
        interaction = a.data['im'].new_interaction()
        interaction.add_person(player, True)
        interaction.add_person(slave)
        interaction.set_area('牢房')
        interaction.start()
    a.page()
    a.h('奴隶列表')
    a.t()
    player = a.data['pm'].get_player()
    slave_hash_list = []
    for person_hash in player.data['关系'].keys():
        if '被囚禁者' in player.data['关系'][person_hash]['关系']:
            slave_hash_list.append(person_hash)
    slave_list = []
    for person_hash in slave_hash_list:
        slave_list.append(a.data['pm'].get_person(person_hash))
    for slave in slave_list:
        a.b(slave.data['name'], a.goto, decide, slave)
        a.t()
    a.b('返回', a.back)
