import random
import erajs.api as a


class InteractionManager():
    def new_interaction(self):
        interaction = Interaction()
        return interaction


class Interaction():
    def __init__(self):
        self.data = {
            'scene': '',
            'person': [],
            'order': 0
        }

    def add_person(self, person, group=0):
        self.data['person'].append([person, group, []])

    def set_scene(self, scene):
        self.data['scene'] = scene

    def start(self, quiet=True):
        """
        动作按下
        检测可行性
        输出动作细节
        计算动作影响
        触发口上
        显示动作影响
        （计算被触发事件-
        计算事件影响-
        触发口上-
        显示事件影响）
        计算结束回合事件
        显示属性值变更
        """

        def next_order():
            '''
            计算下一个索引的值\n
            '''
            self.data['order'] += 1
            if self.data['order'] >= len(self.data['person']):
                self.data['order'] = 0

        def get_person_group(person):
            for each in self.data['person']:
                if each[0] == person:
                    return each[1]

        def turn_core():
            a.page()
            a.t('【{}的回合】'.format(self.data['person']
                                 [self.data['order']][0].data['name']))
            a.t()
            while not self.data['person'][self.data['order']][0].is_player():
                ai_turn()
                check_die()
                res = check_end()
                if res == None:
                    next_order()
                else:
                    a.t('探险结束', color='#f00')
                    while not a.get_gui_list()[-2] == 'loop':
                        a.clear_gui(1)
                    break
            a.back()

        def check_die():
            for each in self.data['person']:
                if each[0].data['体力'] <= 0 and not '死亡' in each[2]:
                    each[2].append('死亡')
                    a.t('{}失去战斗能力。'.format(each[0].data['name']), True)
                    a.t()
                    a.t('{}身上的{}金钱和{}点经验将被对方平分。'.format(each[0].data['name'],
                                                        each[0].data['money'],
                                                        each[0].data['exp']), True)
                    a.t()
                    # 其他组角色分战利品
                    person_group = get_person_group(each[0])
                    enemy_group = []
                    for every in self.data['person']:
                        if not every[1] == person_group:
                            enemy_group.append(every[0])
                    for every in enemy_group:
                        add_exp = int(each[0].data['exp'] / len(enemy_group))
                        add_money = int(each[0].data['money']/len(enemy_group))
                        a.t('{}获得了{}点经验和{}金钱！'.format(
                            every.data['name'], add_exp, add_money))
                        a.t()
                        every.data['exp'] += add_exp
                        every.data['money'] += add_money
                        every.update()

        def check_end():
            groups_set = set()
            for each in self.data['person']:
                if not '死亡' in each[2]:
                    groups_set.add(each[1])
            if len(groups_set) == 1:  # 剩余1组
                return groups_set.pop()
            else:
                return None

        def ai_turn():
            if self.data['scene'] == '战斗':
                sub = self.data['person'][self.data['order']][0]
                sub_group = get_person_group(sub)
                # 确定敌我
                enemy_list = []
                company_list = []
                for person in self.data['person']:
                    if not person[1] == sub_group:
                        enemy_list.append(person[0])
                    else:
                        company_list.append(person[0])
                # 指定随机敌人
                obj = random.choice(enemy_list)
                # 指定随机动作
                act_list = []
                for key in a.data['data.技能'].keys():
                    act = a.data['data.技能'][key]
                    if sub.data['武器'] in act['依赖']['武器']:
                        act_list.append(act)
                act = random.choice(act_list)
                # 施放
                # 计算伤害
                weapon = a.data['data.物品'][sub.data['武器']]
                dmg = sub.data['力量']*5 + random.randint(0, sub.data['力量']*3)
                dmg += weapon['攻击力'] + random.randint(0, weapon['攻击力上浮'])
                dmg += act['攻击力'] + random.randint(0, act['攻击力上浮'])
                obj.data['体力'] -= dmg
                sub.save()
                obj.save()
                # 随机技能文本
                text = random.choice(act['文本'])
                kojo_dic = a.data['km'].get(sub, obj)
                kojo_dic['dmg'] = dmg
                a.t(text.format(**kojo_dic), True)

            elif self.data['scene'] == '酒馆':
                pass

        def ui_player_turn():
            if self.data['person'][self.data['order']][0] == a.data['pm'].get_player():
                a.divider()
                a.t()
                enemy_list = []
                company_list = []
                player = a.data['pm'].get_player()
                player_group = get_person_group(player)
                for person in self.data['person']:
                    if not person[1] == player_group:
                        enemy_list.append(person[0])
                    else:
                        company_list.append(person[0])
                a.t('对方:')
                for enemy in enemy_list:
                    a.b(enemy.data['name'], a.goto, ui_act, enemy)
                a.t()
                a.t('己方:')
                for company in company_list:
                    a.b(company.data['name'], a.goto, ui_act, company)
                a.t()
                if self.data['scene'] == '战斗':
                    a.b('逃跑', a.back)
                else:
                    a.b('离开', a.back)

            else:
                next_order()
                a.repeat()

        def ui_act(person):
            a.page()
            a.h('选择对{}进行的动作'.format(person.data['name']))
            a.t()
            if person.is_player():
                a.b('魔法', a.goto, ui_magic, person)
                a.b('调教', a.goto, ui_love, person, disabled=True)
                a.b('物品', a.goto, ui_item, person)
            else:
                a.b('社交', a.goto, ui_chat, person)
                a.b('战斗', a.goto, ui_fight, person)
                a.b('魔法', a.goto, ui_magic, person)
                a.b('调教', a.goto, ui_love, person, disabled=True)
                a.b('命令', a.goto, ui_order, person)
                a.b('物品', a.goto, ui_item, person)
            a.t()
            a.b('返回', a.back)

        # TOOL
        def before_act(sub, obj, act):  # 动作描述
            a.clear_gui(1)
            a.append_gui(before_kojo, sub, obj, act)
            if act in a.data['act']:  # 有脚本
                a.goto(a.data['act'][act]['main'],
                       sub, obj, self)
            else:  # 没有脚本，使用通用动作描述
                a.goto(a.data['act']['default']['main'],
                       sub, obj, act, self)

        def before_kojo(sub, obj, act):  # 口上
            a.clear_gui(1)
            a.append_gui(final)
            if obj.data['种族'] in a.data['kojo']:  # 有脚本
                a.goto(a.data['kojo'][obj.data['种族']], sub, obj, act, self)
            else:  # 没有脚本，使用通用动作描述
                a.goto(a.data['kojo']['default'], sub, obj, act, self)

        def final():
            check_die()
            res = check_end()
            if res == None:
                a.clear_gui(1)
                next_order()
                a.goto(turn_core)
            else:
                a.t('探险结束', color='#f00')
                while not a.get_gui_list()[-2] == 'loop':
                    a.clear_gui(1)
        # TOOL END

        def ui_chat(person):
            a.page()
            a.h('与{}社交'.format(person.data['name']))
            a.t()
            self.show_hud(a.data['pm'].get_player(), person)
            a.t()
            for act in a.data['data.社交']:
                a.b(act, a.goto, before_act,
                    a.data['pm'].get_player(), person, act)
            a.t()
            a.b('返回', a.back)

        def ui_fight(person):
            a.page()
            a.h('与{}战斗'.format(person.data['name']))
            a.t()
            self.show_hud(a.data['pm'].get_player(), person)
            a.t()
            for act in a.data['data.技能']:
                a.b(act, a.goto, before_act,
                    a.data['pm'].get_player(), person, act)
            a.t()
            a.b('返回', a.back)

        def ui_magic(person):
            a.page()
            a.h('向{}释放魔法'.format(person.data['name']))
            a.t()
            self.show_hud(a.data['pm'].get_player(), person)
            a.t()
            for act in a.data['data.魔法']:
                if act in a.data['act'].keys():
                    if a.data['act'][act]['show']():
                        a.b(act, a.goto, before_act, a.data['pm'].get_player(
                        ), person, act, disabled=not a.data['act'][act]['active']())
                else:
                    a.b(act, None, disabled=True)
            a.t()
            a.b('返回', a.back)

        def ui_love(person):
            a.page()
            a.h('调教{}'.format(person.data['name']))
            a.t()
            self.show_hud(a.data['pm'].get_player(), person)
            a.t()
            for act in a.data['data.互动']['分类']['社交']:
                if act in a.data['act'].keys():
                    if a.data['act'][act]['show']():
                        a.b(act, a.goto, before_act, a.data['pm'].get_player(
                        ), person, act, disabled=not a.data['act'][act]['active']())
                else:
                    a.b(act, None, disabled=True)
            a.t()
            a.b('返回', a.back)

        def ui_order(person):
            a.page()
            a.h('命令{}'.format(person.data['name']))
            a.t()
            self.show_hud(a.data['pm'].get_player(), person)
            a.t()
            for act in a.data['data.命令']:
                if act in a.data['act'].keys():
                    if a.data['act'][act]['show']():
                        a.b(act, a.goto, before_act, a.data['pm'].get_player(
                        ), person, act, disabled=not a.data['act'][act]['active']())
                else:
                    a.b(act, None, disabled=True)
            a.t()
            a.b('返回', a.back)

        def ui_item(person):
            a.page()
            a.h('对{}使用物品'.format(person.data['name']))
            a.t()
            self.show_hud(a.data['pm'].get_player(), person)
            a.t()
            for act in a.data['data.物品']:
                if act in a.data['act'].keys():
                    if a.data['act'][act]['show']():
                        a.b(act, a.goto, before_act, a.data['pm'].get_player(
                        ), person, act, disabled=not a.data['act'][act]['active']())
                else:
                    a.b(act, None, disabled=True)
            a.t()
            a.b('返回', a.back)

        def act_loop():
            sub = a.data['pm'].get_person(self.data['sub'])
            obj = []
            for person in self.data['person']:
                if not person.data['hash'] == self.data['sub']:
                    obj.append(person)
            if len(obj) == 1:
                obj = obj[0]
                a.divider()
                self.show_hud(sub, obj)
                a.divider()
                if sub.is_player():
                    for act in a.data['data.互动'].keys():
                        if act in a.data['act'].keys():
                            a.b(act, self.act, sub, act, obj, self.data)
                    a.t()
                    a.b('返回', a.back)

        def current_act():
            if self.data['person'][self.data['order']][0] == a.data['pm'].get_player():
                a.divider()
                a.t()
                enemy_list = []
                company_list = []
                player = a.data['pm'].get_player()
                player_group = get_person_group(player)
                for person in self.data['person']:
                    if not person[1] == player_group:
                        enemy_list.append(person[0])
                    else:
                        company_list.append(person[0])
                a.t('对方:')
                for enemy in enemy_list:
                    a.b(enemy.data['name'], a.goto, ui_act, enemy)
                a.t()
                a.t('己方:')
                for company in company_list:
                    a.b(company.data['name'], a.goto, ui_act, company)
                a.t()
                a.b('返回', a.back)
            else:
                next_order()
                current_act()
        # 初始化战斗
        self.data['person'].sort(key=lambda person: 1/person[0].data['敏捷'])
        a.append_gui(ui_player_turn)
        a.goto(turn_core)

    def act(self, sub, act, obj, data):
        a.page()
        a.data['act'][act](sub, obj, self.data)
        # a.data['kojo'][obj.data['种族']](sub, act, obj, self.data)
        a.repeat()

    def show_hud(self, sub, obj):
        def set_fav(data):
            pass
        p = a.data['km'].get(sub, obj)
        a.t('对方:{obj_name}'.format(**p))
        a.t('　好感:{}'.format(obj.think(sub)['好感']))
        a.t('　收藏:')
        a.rate(0, 1, set_fav, False)
        a.t()
        a.t('种族：{}'.format(obj.data['种族']))
        a.t()
        a.t('【服装】')
        a.t()
        a.t('体力:({}/{})'.format(obj.data['体力'], obj.data['体力上限']))
        a.progress(obj.data['体力'], obj.data['体力上限'], 120)
        a.t('　耐力:({}/{})'.format(obj.data['耐力'], obj.data['耐力上限']))
        a.progress(obj.data['耐力'], obj.data['耐力上限'], 120)
        a.t('　精力:({}/{})'.format(obj.data['精力'], obj.data['精力上限']))
        a.progress(obj.data['精力'], obj.data['精力上限'], 120)
        a.t()
        # a.t('快ＶLv{}'.format(obj.data['状态']['快']['v']))
        # a.progress(0, 100, 50)
        # a.t('　快ＡLv{}'.format(obj.data['状态']['快']['a']))
        # a.progress(0, 100, 50)
        # a.t('　快ＵLv{}'.format(obj.data['状态']['快']['u']))
        # a.progress(0, 100, 50)
        # a.t('　快ＣLv{}'.format(obj.data['状态']['快']['c']))
        # a.progress(0, 100, 50)
        # a.t('　快ＢLv{}'.format(obj.data['状态']['快']['b']))
        # a.progress(0, 100, 50)
        # a.t('　快ＭLv{}'.format(obj.data['状态']['快']['m']))
        # a.progress(0, 100, 50)
        # a.t()
        # a.t('Ｖ润Lv{}'.format(obj.data['状态']['润']['v']))
        # a.progress(0, 100, 50)
        # a.t('　Ａ润Lv{}'.format(obj.data['状态']['润']['a']))
        # a.progress(0, 100, 50)
        # a.t('　Ｕ润Lv{}'.format(obj.data['状态']['润']['u']))
        # a.progress(0, 100, 50)
        # a.t('　顺从Lv{}'.format(obj.data['状态']['快']['b']))
        # a.progress(0, 100, 50)
        # a.t('　欲情Lv{}'.format(obj.data['状态']['快']['m']))
        # a.progress(0, 100, 50)
        # a.t('　屈服Lv{}'.format(obj.data['状态']['快']['u']))
        # a.progress(0, 100, 50)
        # a.t()
        # a.t('屈辱Lv{}'.format(obj.data['状态']['快']['c']))
        # a.progress(0, 100, 50)
        # a.t('　习得Lv{}'.format(obj.data['状态']['快']['v']))
        # a.progress(0, 100, 50)
        # a.t('　苦痛Lv{}'.format(obj.data['状态']['快']['a']))
        # a.progress(0, 100, 50)
        # a.t('　恐怖Lv{}'.format(obj.data['状态']['快']['b']))
        # a.progress(0, 100, 50)
        # a.t('　反感Lv{}'.format(obj.data['状态']['快']['m']))
        # a.progress(0, 100, 50)
        # a.t('　抑郁Lv{}'.format(obj.data['状态']['快']['u']))
        # a.progress(0, 100, 50)
        # a.t()
        # a.t('射精:({}/{})'.format(100, 100))
        # a.progress(0, 100, 230)
        # a.t('　喷乳:({}/{})'.format(100, 100))
        # a.progress(0, 100, 230)
        # a.t()
        # a.t('性欲:({}/{})'.format(100, 100))
        # a.progress(0, 100, 50)
        # a.t('　精欲:({}/{})'.format(100, 100))
        # a.progress(0, 100, 50)
        # a.t('　体内精量:{}ml'.format(100))
        # a.progress(0, 100, 50)
        # a.t('　尿意:')
        # a.progress(0, 100, 50)
        # a.t('　厕欲:')
        # a.progress(0, 100, 50)
        # a.t()
        # a.t()
        a.t('{sub_name}'.format(**p))
        a.t('　体力:({}/{})'.format(sub.data['体力'], sub.data['体力上限']))
        a.progress(sub.data['体力'], sub.data['体力上限'], 110)
        a.t('　耐力:({}/{})'.format(sub.data['耐力'], sub.data['耐力上限']))
        a.progress(sub.data['耐力'], sub.data['耐力上限'], 110)
        a.t('　精力:({}/{})'.format(sub.data['精力'], sub.data['精力上限']))
        a.progress(sub.data['精力'], sub.data['精力上限'], 110)
        # a.t()
        # a.t('性欲:({}/{})'.format(100, 100))
        # a.progress(0, 100, 230)
        # a.t('　射精:({}/{})'.format(100, 100))
        # a.progress(0, 100, 230)
