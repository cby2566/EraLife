import random
import erajs.api as a
# import game.世界 as sj
# import game.地点 as dd
# import game.人物 as rw


class BattleManager():
    """
    “游戏”是唯一的，负责管理玩家的体验。

    同一个游戏中可以存在不同的世界，不同的世界可以拥有不同的地点、不同的人等等.
    """

    def __init__(self):
        pass

    def new_battle(self):
        battle = Battle()
        return battle


class Battle():
    def __init__(self):
        self.data = {
            'battle_group': {}
        }

    def add_person(self, person, group_name):
        if group_name in self.data['battle_group'].keys():
            self.data['battle_group'][group_name].append(person.data['hash'])
        else:
            self.data['battle_group'][group_name] = [person.data['hash']]

    def start_battle(self, show_text=True):
        def battle_loop():
            priority_list.sort(key=lambda pair: pair[1])
            tmp = priority_list[0][1]
            for each in priority_list:
                each[1] -= tmp
            # 确定目标范围
            targets = []
            for group in self.data['battle_group'].keys():
                if not priority_list[0][0].data['hash'] in self.data['battle_group'][group]:
                    targets.extend(self.data['battle_group'][group])
            # 确定目标
            target = random.choice(targets)
            target = a.data['pm'].get_person(target)
            # 查找技能
            skill_list = []
            for each in list(a.data['data.技能'].keys()):
                if priority_list[0][0].data['武器'] in a.data['data.技能'][each]['依赖']['武器']:
                    skill_list.append(each)
            if priority_list[0][0].is_player():
                a.divider()
                for each in skill_list:
                    a.b(each, skill, each, target)
            else:
                # 随机使用技能
                tmp = random.choice(skill_list)
                skill(tmp, target)

        def skill(tmp, target):
            if priority_list[0][0].is_player():
                a.page()
            # 随机技能文本
            text = random.choice(a.data['data.技能'][tmp]['文本'])
            # 计算伤害
            sub = priority_list[0][0]
            dmg = sub.data['力量']*5+random.randint(0, sub.data['力量']*3)
            dmg += a.data['data.物品'][sub.data['武器']]['攻击力'] + \
                random.randint(0, a.data['data.物品'][sub.data['武器']]['上浮'])
            dmg += a.data['data.技能'][tmp]['攻击力'] + \
                random.randint(0, a.data['data.技能'][tmp]['上浮'])
            target.data['体力'] -= dmg
            sub.save()
            target.save()
            # 显示文本
            key = {'sub': sub.data['name'],
                   'obj': target.data['name'],
                   'dmg': dmg,
                   'weapon': sub.data['武器']}
            if show_text:
                a.t()
                a.t(text.format(**key))
            # 判定死亡
            for each in priority_list:
                if each[0].data['体力'] <= 0:
                    if show_text:
                        a.t()
                        a.t('{}失去战斗能力。'.format(each[0].data['name']), True)
                        a.t()
                        a.t('{}身上的{}金钱和{}点经验将被对方平分。'.format(each[0].data['name'],
                                                            each[0].data['money'],
                                                            each[0].data['exp']), True)
                        a.t()
                    benefit_person_hash = []
                    # 其他组角色分战利品
                    for group in self.data['battle_group'].keys():
                        if not each[0].data['hash'] in self.data['battle_group'][group]:
                            benefit_person_hash.extend(
                                self.data['battle_group'][group])
                    for every in benefit_person_hash:
                        every = a.data['pm'].get_person(every)
                        add_exp = int(each[0].data['exp'] /
                                      len(benefit_person_hash))
                        add_money = int(
                            each[0].data['money']/len(benefit_person_hash))
                        if show_text:
                            a.t('{}获得了{}点经验和{}金钱！'.format(
                                every.data['name'], add_exp, add_money))
                        every.data['exp'] += add_exp
                        every.data['money'] += add_money
                        every.update()
                    priority_list.remove(each)
            # 判定胜负
            alive_dict = {}
            for each in priority_list:
                for group in self.data['battle_group'].keys():
                    if each[0].data['hash'] in self.data['battle_group'][group]:
                        if not group in alive_dict.keys():
                            alive_dict[group] = 1
                        else:
                            alive_dict[group] += 1
            count = 0
            for each in alive_dict.keys():
                if not alive_dict[each] == 0:
                    count += 1
                    win_group = each
            if count <= 1:  # 只有一个队伍存活(战斗结束)
                a.data['battle_end'] = True
                if a.data['db']['player'] in self.data['battle_group'][win_group]:
                    # 玩家胜利
                    if random.random() < a.data['data.游戏']['抓捕概率']:
                        catch()
                    else:
                        a.back()
                else:
                    a.back()
            else:  # 继续
                # 刷新优先列表
                priority_list[0][1] += 1/priority_list[0][0].data['敏捷']
                a.repeat()

        def catch():
            def tmp():
                a.page()
                a.t('从敌人的尸体中，你发现一个人尚有一丝气息，要治疗并监禁他吗？')
                a.t()
                a.b('查看人物', a.goto,
                    a.data['pm'].show_profile, slave)
                a.b('是', confirm)
                a.b('否', a.back, 2)

            def confirm():
                a.page()
                a.t('你将他五花大绑扛回了家，并囚禁了起来。')
                a.t('{}感谢你的不杀之恩，好感+5}'.format(slave.data['name']))
                a.t()
                slave.think(a.data['pm'].get_player())['关系'].append('囚禁者')
                slave.save()
                a.data['pm'].get_player().think(slave)['关系'].append('被囚禁者')
                a.data['pm'].get_player().save()
                a.b('返回', a.back, 2)
            targets = []
            for each in self.data['battle_group'].keys():
                if not a.data['db']['player'] in self.data['battle_group'][each]:
                    for each in self.data['battle_group'][each]:
                        targets.append(each)
            slave = random.choice(targets)
            slave = a.data['pm'].get_person(slave)
            slave.data['体力'] = 1
            slave.save()
            a.goto(tmp)
        # 生成优先列表
        priority_list = []
        for group in self.data['battle_group'].keys():
            for person_hash in self.data['battle_group'][group]:
                person = a.data['pm'].get_person(person_hash)
                priority_list.append([person, 1/person.data['敏捷']])
        # 确定战斗顺序
        a.goto(battle_loop)
