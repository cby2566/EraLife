import erajs.api as a
import logic.世界 as sj
import logic.地点 as dd
import logic.人物 as rw
import logic.战斗 as zd
import logic.互动 as hd
import logic.口上 as ks
import logic.wiki as wiki


class GameManager():
    """
    “游戏”是唯一的，负责管理玩家的体验。

    同一个游戏中可以存在不同的世界，不同的世界可以拥有不同的地点、不同的人等等.
    """

    def __init__(self):
        worlds = {}
        # 初始化人物管理器
        a.data['pm'] = rw.PersonManager()
        # 初始化世界管理器
        a.data['wm'] = sj.WorldManager()
        # 初始化战斗管理器
        a.data['bm'] = zd.BattleManager()
        # 初始化互动管理器
        a.data['im'] = hd.InteractionManager()
        # 初始化口上管理器
        a.data['km'] = ks.KojoManager()
        # 初始化Wiki
        a.data['wiki'] = wiki.Wiki()

    def new_game(self):
        world = a.data['wm'].new_world()
        world.save()
        a.data['pm'].generate_init_person()
