import random
import erajs.api as a
from . import 姓名 as xm


class PersonManager():
    pool = []

    def __init__(self):
        a.data['db']['person'] = {}

    def add_person(self, person):
        self.pool.append(person)
        person.save()

    def new_null_person(self):
        person = Person()
        self.add_person(person)
        return person

    def load_all_person(self):
        self.pool = []
        for person_hash in a.data['db']['person'].keys():
            person = Person()
            person.load(person_hash)
            self.pool.append(person)

    def save_all_person(self):
        for person in self.pool:
            person.save()

    def new_random_person(self, level=1, up_to=1, gender=''):
        person = self.new_null_person()
        a.data['db']['person'].pop(person.data['hash'])  # 待优化
        person.random(level, up_to, gender)
        return person

    def get_person(self, person_hash):
        for person in self.pool:
            if person.data['hash'] == person_hash:
                return person

    def get_player(self):
        for person in self.pool:
            if person.data['hash'] == a.data['db']['player']:
                return person

    def set_person(self):
        pass

    def show_person_profile(self, person):
        data = person.data
        a.t('{}　{}　Lv:{}　EXP:({}/{})　金钱:￥{}'.format(data['name'],
                                                    data['gender'],
                                                    data['level'],
                                                    data['exp'],
                                                    data['exp_limit'],
                                                    data['money'],))
        a.t()
        a.t('体力:({}/{})'.format(data['体力'],
                                data['体力上限']))
        a.t('　耐力:({}/{})'.format(data['耐力'],
                                 data['耐力上限']))
        a.t('　精力:({}/{})'.format(data['精力'],
                                 data['精力上限']))
        a.t()
        a.t('力量:{}　敏捷:{}　智力:{}　未使用属性点:{}'.format(data['力量'],
                                                 data['敏捷'],
                                                 data['智力'],
                                                 data['属性点']))
        a.t()
        a.t('武器:[{}]'.format(data['武器']))

    def produce(self, person1, person2):
        children = []
        for i in range(random.randint(3)):
            child = self.new_random_person(1, 0)
            child.data['父亲'] = person1.data['hash']
            person1.data['后代'].append(child.data['hash'])
            child.data['母亲'] = person2.data['hash']
            person2.data['后代'].append(child.data['hash'])
            child.save()

    def generate_init_person(self):
        for world_hash in a.data['db']['world'].keys():
            world = a.data['wm'].get_world(world_hash)
            for y, place_line in enumerate(world.data['map']):
                for x, place in enumerate(place_line):
                    for i in range(random.randint(1, 3)):
                        person = self.new_random_person()
                        place['person'].append(person.data['hash'])
                        person.save()
                    for func_name in a.data['data.地点'][place['type']]['功能'].keys():
                        if func_name == '郊区':
                            for i in range(random.randint(2, 3)):
                                person = self.new_random_person()
                                place['person'].append(person.data['hash'])
                                person.save()
                        if func_name == '市场':
                            person = self.new_random_person()
                            place['person'].append(person.data['hash'])
                            person.data['职业'] = '商店老板'
                            person.save()
                        if func_name == '酒馆':
                            person = self.new_random_person()
                            place['person'].append(person.data['hash'])
                            person.data['职业'] = '酒馆老板'
                            person.save()

    def show_profile(self, person):
        def page1():
            def next_page():
                a.clear_gui(1)
                a.goto(page2)
            a.page()
            a.h('人物档案：战斗')
            a.t()
            a.t('{}　{}　Lv:{}　EXP:({}/{})'.format(person.data['name'],
                                                 person.data['gender'],
                                                 person.data['level'],
                                                 person.data['exp'],
                                                 person.data['exp_limit']))
            a.progress(person.data['exp'], person.data['exp_limit'])
            a.t()
            a.t('体力:({}/{})'.format(person.data['体力'],
                                    person.data['体力上限']))
            a.progress(person.data['体力'],
                       person.data['体力上限'])
            a.t('　耐力:({}/{})'.format(person.data['耐力'],
                                     person.data['耐力上限']))
            a.progress(person.data['耐力'],
                       person.data['耐力上限'])
            a.t('　精力:({}/{})'.format(person.data['精力'],
                                     person.data['精力上限']))
            a.progress(person.data['精力'],
                       person.data['精力上限'])
            a.t()
            a.t('力量:{}　敏捷:{}　智力:{}　'.format(person.data['力量'],
                                            person.data['敏捷'],
                                            person.data['智力']))
            attr_point = person.data['属性点']
            if person.data['hash'] == a.data['db']['player']:
                if attr_point > 0:
                    a.b('属性点:{}'.format(attr_point),
                        a.goto, self.add_attr_point, person)
                else:
                    a.b('属性点:{}'.format(attr_point), None, disabled=True)
                a.b('武器:[{}]'.format(person.data['武器']),
                    a.goto, self.equipment)
                a.b('防具管理', None, disabled=True)
                a.b('服装管理', None, disabled=True)
            a.t()
            a.t('体型:{}　身高：{}cm　体重:{}kg　三围:{}/{}/{}'.format('N/A',
                                                           person.data['身高'],
                                                           person.data['体重'],
                                                           person.data['胸围'],
                                                           person.data['腰围'],
                                                           person.data['臀围']))
            a.t()
            a.t('属性:')
            a.t()
            a.t('经验:')
            a.t()
            a.t('异常经验:')
            a.t()
            a.t('参数:')
            a.t()
            a.t('刻印:')
            a.t()
            a.t('素质:')
            a.t()
            a.t('性别:')
            a.t()
            a.t('种族:')
            a.t()
            a.b('下一页', next_page)
            a.b('返回', a.back)

        def page2():
            def last_page():
                a.clear_gui(1)
                a.goto(page1)
            a.page()
            a.h('人物档案：交互')
            a.t()
            a.t('职业:')
            a.t()
            a.t('性格:')
            a.t()
            a.t('体质:')
            a.t()
            a.t('技能:')
            a.t()
            a.t('魔法:')
            a.t()
            a.t('后天:')
            a.t()
            a.t('弱点:')
            a.t()
            a.t('助手职责:')
            a.t()
            a.t('指示:')
            a.t()
            a.t('服装:')
            a.t()
            a.t('关系:')
            a.t()
            a.t('作品:')
            a.t()
            a.t('榨精排行:')
            a.t()
            a.b('上一页', last_page)
            a.b('返回', a.back)
        page1()

    def add_attr_point(self, person):
        def add_attr(x):
            if person.data['属性点'] > 0:
                person.data['属性点'] -= 1
                person.data[x] += 1
                a.repeat()
            else:
                a.page()
                a.t('没有未使用的属性点！', True)
                a.repeat()

        a.page()
        a.h('待分配属性点：{}'.format(person.data['属性点']))
        a.t()
        a.t('力量:{}　'.format(person.data['力量']))
        a.b('+', add_attr, '力量')
        a.t()
        a.t('敏捷:{}　'.format(person.data['敏捷']))
        a.b('+', add_attr, '敏捷')
        a.t()
        a.t('智力:{}　'.format(person.data['智力']))
        a.b('+', add_attr, '智力')
        a.t()
        a.b('返回', a.back)

    def equipment(self):
        def change_equipment(pos):
            a.page()
            a.t('{}修改为：'.format(pos))
            a.t()
            print(pm.get_player().data)
            for each in pm.get_player().data['物品']:
                print(a.data['data.物品'][each]['类型'], pos)
                if a.data['data.物品'][each]['类型'] == pos:
                    a.b(each, confirm, pos, each)
            a.t()
            a.b('返回', a.back)

        def confirm(pos, item):
            a.page()
            a.t('确定要将{}修改为{}吗？'.format(pos, item))
            a.t()
            a.b('是', change, pos, item)
            a.t('/')
            a.b('否', a.back)

        def change(pos, item):
            a.page()
            pm.get_person(a.data['db']['player']).data[pos] = item
            a.t('你已将{}替换为{}。'.format(pos, item), True)
            # print(pm.get_person(a.data['db']['player']).data[pos])
            a.back()

        a.page()
        a.h('装备管理')
        a.t()
        pm = a.data['pm']
        a.b('武器：[{}]'.format(pm.get_person(a.data['db']['player']).data['武器']),
            a.goto, change_equipment, '武器')
        a.t()
        a.b('返回', a.back)


class Person():

    def __init__(self):
        self.data = {
            # 系统
            'name': 'N/A',
            'hash': a.new_hash(),
            'tag': [],
            # 称呼
            '系统称呼': 'N/A',
            '自称': '我',
            '尊称': '您',
            '蔑称': '你',
            '第三人称': '他',
            # 灵魂
            'level': 1,
            'exp': 0,
            'exp_limit': 100,
            'buff': [],
            'money': 0,
            '善恶': 0,
            '职业': '',
            '智力': 1,
            '性格': [],
            '思想': [],
            '技能': [],
            '履历': [],
            '刻印': [],
            '属性点': 0,
            '关系': {},
            # 身体
            'gender': '男',
            'age': 0,
            '生日': 0,
            '种族': '人类',
            '身高': 150,
            '体重': 50,
            '胸围': 50,
            '腰围': 50,
            '臀围': 50,
            '力量': 1,
            '敏捷': 1,
            '体力': 100,
            '耐力': 100,
            '精力': 100,
            '体力上限': 100,
            '耐力上限': 100,
            '精力上限': 100,
            '状态': {
                '快': {
                    'c': 0,
                    'v': 0,
                    'a': 0,
                    'b': 0,
                    'm': 0,
                    'u': 0,
                },
                '润': {
                    'v': 0,
                    'a': 0,
                    'u': 0,
                },
                '苦痛': 0,
                '耻辱': 0,
                '惊恐': 0,
                '屈服': 0,
                '抑郁': 0,
                '尴尬': 0,
            },
            '素质': {
                '感': {
                    'c': 0,
                    'v': 0,
                    'a': 0,
                    'b': 0,
                    'm': 0,
                    'u': 0,
                },
                '名': {
                    'v': 0,
                    'a': 0,
                    'b': 0,
                    'm': 0,
                    'u': 0,
                },
                '倾向': {
                    's': 0,
                    'm': 0,
                },
                '技术': {
                    'm': 0,
                    'b': 0,
                    'h': 0,
                    'v': 0,
                    'a': 0,
                    'f': 0,
                },
            },
            '特点': [],
            # 服装
            '内衣': '',
            '内裤': '',
            '衣服': '',
            '裤子': '',
            '袜子': '',
            '鞋子': '',
            '帽子': '',
            '面具': '',
            '手套': '',
            '外套': '',
            '项链': '',
            '手镯': '',
            '戒指': '',
            # 装备
            '武器': '',
            '头部': '',
            '面部': '',
            '颈部': '',
            '上身': '',
            '左手': '',
            '右手': '',
            '左指': '',
            '右指': '',
            '下身': '',
            '脚部': '',
            '物品': [],
        }

    def save(self):
        a.data['db']['person'][self.data['hash']] = self.data

    def load(self, hash):
        if hash in a.data['db']['person'].keys():
            self.load_data(a.data['db']['person'][hash])

    def load_data(self, data):
        self.data = data

    def random(self, level=1, up_to=1, gender=''):
        self.data['hash'] = a.new_hash()
        # 随机性别
        if gender == '':
            self.data['gender'] = random.choice(
                list(a.data['data.others']['性别'].keys()))
        else:
            self.data['name'] = gender
        # 随机姓名
        self.data['name'] = xm.get_random_name(self.data['gender'])
        self.data['系统称呼'] = self.data['name']
        self.data['第三人称'] = a.data['data.others']['性别'][self.data['gender']]['默认第三人称']
        # 随机种族
        # self.data['种族'] = random.choice(list(a.data['data.种族'].keys()))
        self.data['种族'] = '人类'
        # 随机等级
        self.data['level'] = random.randint(level, level+up_to)
        # 随机力量/敏捷/智力
        for i in range(self.data['level']-1):
            tmp = random.choice(['力量', '敏捷', '智力'])
            self.data[tmp] += 1
        # 随机金钱
        self.data['money'] = random.randint(
            (self.data['level']-1)*10+1, (self.data['level']-1)*20+10)
        # 随机身高
        tmp = random.choice(a.data['data.others']['身高'])
        if not tmp['name'] == '普通':
            self.data['tag'].append(tmp['name'])
        # 随机体型
        tmp = random.choice(a.data['data.others']['体型'])
        if not tmp['name'] == '普通':
            self.data['tag'].append(tmp['name'])
        # 随机体质
        tmp = random.choice(list(a.data['data.others']['体质'].keys()))
        if not tmp == '普通':
            self.data['tag'].append(tmp)
        # 随机经验值
        self.data['exp'] = random.randint(0, self.data['exp_limit'])
        self.update()

    def set_gender(self, gender):
        self.data['gender'] = gender

    def gain_exp(self, exp):
        pass

    def get_item(self, item):
        self.data['物品'].append(item)

    def update(self):
        def limit_value(value, min_value, max_value):
            if value < min_value:
                value = min_value
            elif value > max_value:
                value = max_value
            return value
        # 确定是否升级
        self.data['exp_limit'] = self.data['level']*100
        if self.data['exp'] >= self.data['exp_limit']:
            a.t('{}升级了！'.format(self.data['name']))
            self.data['exp'] -= self.data['exp_limit']
            self.data['属性点'] += 1
            self.data['level'] += 1
            self.update()
        # 计算三力上限
        self.data['体力上限'] = (self.data['level']-1)*50 + \
            (self.data['力量']-1)*50+100
        self.data['耐力上限'] = (self.data['level']-1)*50 + \
            (self.data['敏捷']-1)*50+100
        self.data['精力上限'] = (self.data['level']-1)*50 + \
            (self.data['智力']-1)*50+100
        if '虚弱' in self.data['tag']:
            rate = a.data['data.others']['体质']['虚弱']['rate']
            self.data['体力上限'] *= rate
            self.data['耐力上限'] *= rate
            self.data['精力上限'] *= rate
        if '强壮' in self.data['tag']:
            rate = a.data['data.others']['体质']['强壮']['rate']
            self.data['体力上限'] *= rate
            self.data['耐力上限'] *= rate
            self.data['精力上限'] *= rate
        # 整数化三力上限
        self.data['体力上限'] = int(self.data['体力上限'])
        self.data['耐力上限'] = int(self.data['耐力上限'])
        self.data['精力上限'] = int(self.data['精力上限'])
        # 限制三力
        self.data['体力'] = limit_value(self.data['体力'], 0, self.data['体力上限'])
        self.data['耐力'] = limit_value(self.data['耐力'], 0, self.data['耐力上限'])
        self.data['精力'] = limit_value(self.data['精力'], 0, self.data['精力上限'])
        # 整数化三力
        self.data['体力'] = int(self.data['体力'])
        self.data['耐力'] = int(self.data['耐力'])
        self.data['精力'] = int(self.data['精力'])
        # 修正空手
        if self.data['武器'] == '':
            self.data['武器'] = '空手'

    def reset(self):
        self.data['体力'] = self.data['体力上限']
        self.data['耐力'] = self.data['耐力上限']
        self.data['精力'] = self.data['精力上限']
        self.save()

    def is_player(self):
        if self.data['hash'] == a.data['db']['player']:
            return True
        else:
            return False

    def think(self, person):
        if not person.data['hash'] in self.data['关系'].keys():
            self.data['关系'][person.data['hash']] = {
                '好感': 0,
                '关系': []
            }
        return self.data['关系'][person.data['hash']]

    def be(self, test):
        "检查人物是否是xx"
        ans = False
        if test in self.data['tag']:
            ans = True
        if test == self.data['职业']:
            ans = True
        if test in self.data['性格']:
            ans = True
        if test in self.data['思想']:
            ans = True
        if test in self.data['技能']:
            ans = True
        if test == self.data['种族']:
            ans = True
        if test in self.data['特点']:
            ans = True
        if test == self.data['内衣']:
            ans = True
        if test == self.data['内裤']:
            ans = True
        if test == self.data['衣服']:
            ans = True
        if test == self.data['裤子']:
            ans = True
        if test == self.data['袜子']:
            ans = True
        if test == self.data['鞋子']:
            ans = True
        if test == self.data['帽子']:
            ans = True
        if test == self.data['面具']:
            ans = True
        if test == self.data['手套']:
            ans = True
        if test == self.data['外套']:
            ans = True
        if test == self.data['项链']:
            ans = True
        if test == self.data['手镯']:
            ans = True
        if test == self.data['戒指']:
            ans = True
        if test == self.data['武器']:
            ans = True
        if test == self.data['头部']:
            ans = True
        if test == self.data['面部']:
            ans = True
        if test == self.data['颈部']:
            ans = True
        if test == self.data['上身']:
            ans = True
        if test == self.data['左手']:
            ans = True
        if test == self.data['右手']:
            ans = True
        if test == self.data['左指']:
            ans = True
        if test == self.data['右指']:
            ans = True
        if test == self.data['下身']:
            ans = True
        if test == self.data['脚部']:
            ans = True
        if test in self.data['物品']:
            ans = True
        return ans
