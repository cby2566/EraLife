import erajs.api as a


class KojoManager:
    def get(self, sub, obj):
        d = {}
        l = [
            'name',
            '系统称呼',
            '自称',
            '尊称',
            '蔑称',
            '第三人称',
            '职业',
            'gender',
            'age',
            '种族',
            '身高',
            '体重',
            '胸围',
            '腰围',
            '臀围',
            '内衣',
            '内裤',
            '衣服',
            '裤子',
            '袜子',
            '鞋子',
            '帽子',
            '面具',
            '手套',
            '外套',
            '项链',
            '手镯',
            '戒指',
            '武器',
            '头部',
            '面部',
            '颈部',
            '上身',
            '左手',
            '右手',
            '左指',
            '右指',
            '下身',
            '脚部',
        ]
        for each in l:
            d['sub_'+each] = sub.data[each]
            d['obj_'+each] = obj.data[each]
        return d

    def say_kojo(self):
        pass
