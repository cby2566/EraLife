# coding:utf-8
import random
import erajs.api as a
# 注意！不推荐在此处定义变量，而应在 a.init() 之后将变量定义在 a.data['db'] 中，以获得数据持久性支持。

_version = '0.1.0'


def cover():
    a.title('EraEditor v{} with Era.js v{}'.format(_version, a.version))
    a.page()
    a.h('EraEditor v{}'.format(_version))
    a.t('with Era.js v{}'.format(a.version))
    a.t()
    a.t()
    a.b('元数据编辑器', a.goto, ui_metadata_editor, popup='为各种游戏数据制定标准')
    a.t()
    a.b('游戏数据编辑器', a.goto, ui_game_data_editor, popup='管理游戏数据')
    a.t()
    a.b('数据文件编辑器', a.goto, ui_data_file_editor, popup='管理游戏数据文件')
    a.t()
    a.b('存档编辑器', a.goto, ui_save_editor, popup='管理存档数据')
    a.t()
    a.b('编辑器设置', a.goto, ui_editor_config)
    a.t()
    a.b('退出编辑器', a.back)


def ui_metadata_editor():
    """元数据编辑器"""
    def ui_detail_editor(key):
        a.page()
        a.h('【{}】元数据编辑器'.format(key))
        a.t()
        for attr_name in a.data['data.元数据'][key].keys():
            a.b(attr_name, a.goto, ui_change_attr, key, attr_name)
        a.t()
        a.divider()
        a.t()
        a.b('新增属性', a.goto, ui_add_attr, key)
        a.t()
        a.b('返回', a.back)

    def ui_add_attr(key):
        def change_attr_name(new_name):
            nonlocal attr_name
            attr_name = new_name

        def change_attr_type(new_type):
            nonlocal attr_type
            attr_type = new_type

        def change_attr_default(new_default):
            nonlocal attr_default
            attr_default = new_default

        def submit():
            a.data['data.元数据'][key][attr_name] = {
                'type': attr_type,
                'default': attr_default,
            }
            a.save_data_to_file('data.元数据')
            for each in a.data['data.'+key]:
                if attr_name not in a.data['data.'+key][each]:
                    a.data['data.'+key][each][attr_name] = convert_input_to_value(
                        attr_default, attr_type)
            a.save_data_to_file('data.'+key)
            a.back()
        a.page()
        a.h('新增属性')
        a.t()
        attr_name = ''
        a.t('新增属性名称：')
        a.input(change_attr_name)
        a.t()
        attr_type = 'bool'
        a.t('属性类型：')
        a.radio(['bool', 'int', 'str', 'list', 'dict'], 0, change_attr_type)
        a.t()
        attr_default = ''
        a.t('默认值：')
        a.input(change_attr_default)
        a.t()
        a.b('确定', submit)
        a.b('返回', a.back)

    def ui_change_attr(key, attr):
        def change_attr_name(new_name):
            nonlocal attr_name
            attr_name = new_name

        def change_attr_type(new_type):
            nonlocal attr_type
            attr_type = new_type

        def change_attr_default(new_default):
            nonlocal attr_default
            attr_default = new_default

        def del_attr():
            del a.data['data.元数据'][key][attr]
            a.save_data_to_file('data.元数据')
            for each in a.data['data.'+key]:
                if attr in a.data['data.'+key][each]:
                    del a.data['data.'+key][each][attr]
            a.save_data_to_file('data.'+key)
            a.back()

        def submit():
            if not attr == attr_name:
                # 重命名
                del a.data['data.元数据'][key][attr]
                for each in a.data['data.'+key]:
                    tmp = a.data['data.'+key][each][attr]
                    del a.data['data.'+key][each][attr]
                    a.data['data.'+key][each][attr_name] = tmp
                a.save_data_to_file('data.'+key)
            a.data['data.元数据'][key][attr_name] = {
                'type': attr_type,
                'default': attr_default,
            }
            a.save_data_to_file('data.元数据')
            a.back()
        a.page()
        a.h('修改【{}】属性'.format(attr))
        a.t()
        a.t()
        a.t('新属性名称：')
        attr_name = attr
        a.input(change_attr_name, attr_name)
        a.t()
        attr_type = a.data['data.元数据'][key][attr]['type']
        candidate = ['bool', 'int', 'str', 'list', 'dict']
        i = candidate.index(attr_type)
        a.t('新属性类型：')
        a.radio(candidate, i, change_attr_type)
        a.t()
        attr_default = a.data['data.元数据'][key][attr]['default']
        a.t('新默认值：')
        a.input(change_attr_default, attr_default)
        a.t()
        a.divider()
        a.t()
        a.b('删除属性', del_attr, color='red')
        a.t()
        a.b('确定', submit)
        a.b('返回', a.back)

    def generate():
        metadata = {}
        for key in a.data['data.元数据'].keys():
            # 一个项目集拥有不同子项但只有一种元数据
            data = a.data['data.'+key]
            dataset = []
            for item_name in data.keys():
                for k in data[item_name].keys():
                    pair = [k, type(data[item_name][k]).__name__]
                    if pair not in dataset:
                        dataset.append(pair)
            metadata[key] = dataset
        print(metadata)
        new_metadata = {}
        for key in metadata.keys():
            new_metadata[key] = {}
            for attr in metadata[key]:
                new_metadata[key][attr[0]] = {
                    'type': attr[1]
                }
                if attr[1] == 'bool':
                    new_metadata[key][attr[0]]['default'] = False
                elif attr[1] == 'int':
                    new_metadata[key][attr[0]]['default'] = 0
                elif attr[1] == 'str':
                    new_metadata[key][attr[0]]['default'] = ''
                elif attr[1] == 'list':
                    new_metadata[key][attr[0]]['default'] = []
                elif attr[1] == 'dict':
                    new_metadata[key][attr[0]]['default'] = {}
        print(new_metadata)
        a.data['data.元数据'] = new_metadata

    def save():
        a.save_data_to_file('data.元数据')

    def apply():
        for meta_name in a.data['data.元数据'].keys():
            for attr_name in a.data['data.元数据'][meta_name].keys():
                for data_name in a.data['data.'+meta_name].keys():
                    # 检查存在
                    if attr_name not in a.data['data.'+meta_name][data_name].keys():
                        if a.data['data.元数据'][meta_name][attr_name]['type'] == 'bool':
                            default = False
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'int':
                            default = 0
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'str':
                            default = ''
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'list':
                            default = []
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'dict':
                            default = {}
                        a.data['data.'+meta_name][data_name][attr_name] = default
                        # print(a.data['data.'+meta_name][data_name])
                    # 检查值的类型
                    if not type(a.data['data.'+meta_name][data_name][attr_name]).__name__ == a.data['data.元数据'][meta_name][attr_name]['type']:
                        if a.data['data.元数据'][meta_name][attr_name]['type'] == 'bool':
                            default = False
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'int':
                            default = 0
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'str':
                            default = ''
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'list':
                            default = []
                        elif a.data['data.元数据'][meta_name][attr_name]['type'] == 'dict':
                            default = {}
                        a.data['data.'+meta_name][data_name][attr_name] = default
                        print(a.data['data.'+meta_name][data_name])
            a.save_data_to_file('data.'+meta_name)
    a.page()
    a.h('元数据编辑器')
    a.t()
    for key in a.data['data.元数据'].keys():
        a.b(key, a.goto, ui_detail_editor, key)
    a.t()
    a.b('生成元数据', generate)
    a.b('保存元数据', save)
    a.b('应用元数据', apply)
    a.b('返回', a.back)


def ui_game_data_editor():
    """游戏数据编辑器"""
    def ui_class_editor(key):
        a.page()
        a.h('【{}】数据编辑器'.format(key))
        a.t()
        for item in a.data['data.{}'.format(key)].keys():
            a.b(item, a.goto, ui_change_item, key, item)
        a.t()
        a.divider()
        a.t()
        a.b('新增数据', a.goto, ui_add_item, key)
        a.t()
        a.b('返回', a.back)

    def ui_change_item(key, item):
        def del_attr():
            del a.data['data.元数据'][key][attr]
            a.save_data_to_file('data.元数据')
            a.back()

        def attr_function_factory(attr):
            attr = attr  # 特异性函数保存值

            def change_attr(value):
                print(attr, value)
                change_dict[attr] = value
            return change_attr

        def submit():
            print(change_dict)
            for attr_name in change_dict.keys():
                a.data['data.'+key][item][attr_name] = change_dict[attr_name]
        a.page()
        a.h('编辑【{}】'.format(item))
        a.t()
        change_dict = {}
        for attr in a.data['data.元数据'][key].keys():
            a.t('{}:　'.format(attr))
            change_attr_func = attr_function_factory(attr)
            if a.data['data.元数据'][key][attr]['type'] == 'bool':
                if a.data['data.'+key][item][attr]:
                    a.radio(['true', 'false'], 0, change_attr_func)
                else:
                    a.radio(['true', 'false'], 1, change_attr_func)
            else:
                a.input(change_attr_func, a.data['data.'+key][item][attr])
            a.t('　类型:{}'.format(a.data['data.元数据'][key][attr]['type']))
            a.t()
        a.t()
        a.divider()
        a.t()
        a.b('删除数据', del_attr, color='red')
        a.t()
        a.b('确定', submit)
        a.b('返回', a.back)

    def ui_add_item(key):
        def change_name(name):
            nonlocal new_name
            new_name = name

        def attr_function_factory(attr):
            attr = attr  # 特异性函数保存值

            def change_attr(value):
                print(attr, value)
                change_dict[attr] = value
            return change_attr

        def submit():
            a.data['data.'+key][new_name] = change_dict
            a.save_data_to_file('data.'+key)
            a.back()
        a.page()
        a.h('新增数据')
        a.t()
        new_name = ''
        change_dict = {}
        for attr in a.data['data.元数据'][key].keys():
            change_dict[attr] = a.data['data.元数据'][key][attr]['default']
        a.t('数据名称:　')
        a.input(change_name)
        a.t()
        for attr in a.data['data.元数据'][key].keys():
            a.t('{}:　'.format(attr))
            change_attr_func = attr_function_factory(attr)
            if a.data['data.元数据'][key][attr]['type'] == 'bool':
                if a.data['data.元数据'][key][attr]['default']:
                    a.radio(['true', 'false'], 0, change_attr_func)
                else:
                    a.radio(['true', 'false'], 1, change_attr_func)
            else:
                a.input(change_attr_func,
                        a.data['data.元数据'][key][attr]['default'])
            a.t('　类型:{}'.format(a.data['data.元数据'][key][attr]['type']))
            a.t()
        a.t()
        a.divider()
        a.t()
        a.t()
        a.b('确定', submit)
        a.b('返回', a.back)
    a.page()
    a.h('游戏数据编辑器')
    a.t()
    for key in a.data['data.元数据'].keys():
        a.b(key, a.goto, ui_class_editor, key)
    a.t()
    a.b('返回', a.back)


def ui_data_file_editor():
    """数据文件编辑器"""
    a.page()
    a.h('数据文件编辑器')
    a.t()
    for each in a.data.keys():
        name_list = each.split('.')
        if name_list[0] == 'data':
            a.b('.'.join(name_list), a.goto, ui_edit_data,
                '.'.join(name_list), a.data['.'.join(name_list)])
    a.t()
    a.b('返回', a.back)


def ui_save_editor():
    """存档编辑器"""
    a.page()
    a.h('存档编辑器')
    a.t()
    for each in a.data.keys():
        name_list = each.split('.')
        if name_list[0] == 'data':
            a.b('.'.join(name_list), a.goto, ui_edit_data,
                '.'.join(name_list), a.data['.'.join(name_list)])
    a.t()
    a.b('返回', a.back)


def ui_editor_config():
    pass


def ui_edit_data(name, data):
    if type(data) == dict:
        ui_edit_dict(name, data)
    elif type(data) == list:
        ui_edit_list(name, data)
    else:
        ui_edit_others(name, data)


def ui_edit_dict(name, d):
    a.page()
    a.h('查看[{}]'.format(name))
    a.t()
    for i, key in enumerate(d.keys()):
        a.b(key, a.goto, ui_edit_data, name+'/'+key, d[key])
        # if i+1 % 200 == 0:
        #     time.sleep(1)
    a.t()
    a.b('新增', a.goto, new_key, name, d)
    a.b('修改', None, disabled=True)
    a.b('删除', None, disabled=True)
    a.t()
    a.b('返回', a.back)


def ui_edit_list(name, l):
    a.page()
    a.h('查看[{}]'.format(name))
    a.t()
    for i, child_l in enumerate(l):
        if type(l[i]) in [str, int, float]:
            a.b(l[i], a.goto, ui_edit_data, name+'/'+str(i), l[i])
        else:
            a.b('[{}]'.format(i), a.goto, ui_edit_data, name+'/'+str(i), l[i])
        # if i+1 % 200 == 0:
        #     time.sleep(1)
    a.t()
    a.b('新增', a.goto, new_item, name, l)
    a.b('修改', None, disabled=True)
    a.b('删除', None, disabled=True)
    a.t()
    a.b('返回', a.back)


def new_key(name, d):
    current_key = ['']
    current_type = ['']

    def set_key(data):
        current_key[0] = data['value']

    def set_value_type(new_type):
        if new_type == '字符串(String)':
            current_type[0] = ''
        elif new_type == '字典(Dict)':
            current_type[0] = {}
        elif new_type == '列表(List)':
            current_type[0] = []

    def confirm():
        d[current_key[0]] = current_type[0]
        print(d)
        a.back()
    a.page()
    a.h('新增子项[{}]'.format(name))
    a.t()
    a.t('请输入新增子项的键(Key):')
    a.input(set_key)
    a.t()
    a.t('请选择新增子项的值(Value)的类型:')
    a.radio(['字符串(String)', '字典(Dict)', '列表(List)'], 0, set_value_type)
    a.t()
    a.b('确认', confirm)


def new_item(name, l):
    a.h('新增子项[{}]'.format(name))
    a.t()


def ui_edit_others(name, data):
    a.page()
    a.h('编辑[{}]'.format(name))
    a.t()
    a.t('{}'.format(data))
    a.t()
    a.b('新增', None, disabled=True)
    a.b('修改', None, disabled=True)
    a.b('删除', None, disabled=True)
    a.t()
    a.b('返回', a.back)


def convert_input_to_value(text, value_type):
    if value_type == 'bool':
        if text.lower() == 'true':
            return True
        elif text.lower() == 'false':
            return False
        else:
            raise ValueError('输入非法bool值')
    elif value_type == 'int':
        try:
            int(text)
            return int(text)
        except ValueError:
            raise ValueError('输入非法int值')
    elif value_type == 'str':
        return text
    elif value_type == 'list':
        return text
    elif value_type == 'dict':
        return text
    return text


a.init()
