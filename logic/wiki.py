import erajs.api as a


class Wiki:
    def ui_wiki(self):
        a.page()
        a.h('百科全书')
        a.t()
        a.b('种族百科', a.goto, self.ui_种族)
        a.t()
        a.b('特殊人物百科', a.goto, self.ui_特殊人物)
        a.t()
        a.b('职业百科', None, disabled=True)
        a.t()
        a.b('性格百科', None, disabled=True)
        a.t()
        a.b('技能百科', None, disabled=True)
        a.t()
        a.b('物品百科', None, disabled=True)
        a.t()
        a.b('属性百科', None, disabled=True)
        a.t()
        a.t()
        a.b('返回', a.back)

    def ui_种族(self):
        def show_种族(name):
            a.page()
            a.h(name)
            a.t()
            for line in a.data['data.种族'][name]['描述']:
                a.t(line)
                a.t()
            a.t()
            a.b('返回', a.back)
        a.page()
        a.h('种族百科')
        a.t()
        for name in a.data['data.种族'].keys():
            a.b(name, a.goto, show_种族, name)
        a.t()
        a.t()
        a.b('返回', a.back)

    def ui_特殊人物(self):
        def show_特殊人物(name):
            a.page()
            a.h(name)
            a.t()
            for line in a.data['data.特殊人物'][name]['描述']:
                a.t(line)
                a.t()
            a.t()
            a.b('返回', a.back)
        a.page()
        a.h('特殊人物百科')
        a.t()
        for name in a.data['data.特殊人物'].keys():
            a.b(name, a.goto, show_特殊人物, name)
        a.t()
        a.t()
        a.b('返回', a.back)
