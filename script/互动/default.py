import sys
import random


def show():  # 显示可行性
    return True


def active():  # 点击可行性
    return True


def main(sub, obj, act, interact):
    global a
    # 行为描述
    a.page()
    p = a.data['km'].get(sub, obj)
    p['act'] = act
    if act in a.data['data.技能']:
        a.t('【{sub_系统称呼}】对【{obj_系统称呼}】使用了【{act}】'.format(**p), True)
        a.t()
        # 计算动作效果
        data = a.data['data.技能'][act]
        if '耐力消耗'in data:
            sub.data['耐力'] -= data['耐力消耗']
        p['dmg'] = data['攻击力']+random.randint(0, data['攻击力上浮'])
        a.t(random.choice(data['文本']).format(**p), True)
        a.t()
    elif act in a.data['data.魔法']:
        a.t('【{sub_系统称呼}】对【{obj_系统称呼}】使用了【{act}】'.format(**p), True)
        a.t()
        # 计算动作效果
        data = a.data['data.魔法'][act]
        if '耐力消耗'in data:
            sub.data['耐力'] -= data['耐力消耗']
        if '精力消耗'in data:
            sub.data['精力'] -= data['精力消耗']
        p['dmg'] = data['攻击力']+random.randint(0, data['攻击力上浮'])
        a.t(random.choice(data['文本']).format(**p), True)
    a.back()


a = sys.argv[0]
a.data['act']['default'] = {
    'show': show,
    'active': active,
    'main': main
}
