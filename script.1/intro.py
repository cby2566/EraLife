# -*- coding: UTF-8 -*-
import core.game as game
import script.base_lib as lib
import script.era_lib as era
import script.hero_lib as hero


def 角色创建方式界面():
    game.clr_cmd(clr_default_flow=False)  # 清除选项
    game.pline()
    l = era.get_item('角色创建方式')
    for i, each in enumerate(era.sort_list_by_name(l, '角色创建方式')):
        text = '[{:0>3}]{}'.format(i+1, each['名称'])
        game.pcmd(text, i+1, each['点击'])
        game.pl()


def newgame_func():
    game.clr_cmd(clr_default_flow=False)  # 清除选项
    game.pline()
    game.pcmd('[001]  创建新玩家角色', 1, newplayer_func)


def newplayer_func(main_func):
    new_player = hero.get_init_hero()
    new_player['身份'] = '主角'

    def set_gender():
        gender = ''

        def deal_func(order):
            game.pline('-')
            gender = ''
            if order == 0:
                gender = '无性'
            if order == 1:
                gender = '男性'
            if order == 2:
                gender = '女性'
            if order == 3:
                gender = '扶她'
            game.pl('玩家性别为：' + str(gender))
            game.pl('是否接受?')
            game.clr_cmd(clr_default_flow=False)  # 清除选项
            # ans = game.askfor_int()
            ans = lib.yes_or_no()
            if ans == True:
                new_player['性别'] = gender
                set_name()
            else:
                set_gender()
        game.clr_cmd(clr_default_flow=False)  # 清除选项
        game.pline()
        game.pl('请选择玩家性别:')
        game.pcmd('[001]  男性', 1, deal_func, 1)
        game.pl()
        game.pcmd('[002]  女性', 2, deal_func, 2)
        game.pl()
        game.pcmd('[003]  扶她', 3, deal_func, 3)
        game.pcmd(' ', 0, deal_func, 0)

    def set_name():
        game.pline()
        game.pl('请输入玩家名称（默认为“你”）:')
        name = game.askfor_str()
        game.pline('-')
        if name == 'skip_one_wait' or name == 'skip_all_wait':
            name = '你'
        game.data['玩家名称'] = name
        game.pl('玩家名称为：' + str(name))
        game.pl('是否接受?')
        ans = lib.yes_or_no()
        if ans == True:
            new_player['姓名'] = name
            # set_outline()
            game.data['人物库'].append(new_player)
            main_func()
        else:
            set_name()

    def set_outline():
        if not '身高' in new_player:
            new_player['身高'] = '普通'

        if not '身材' in new_player:
            new_player['身材'] = '普通'

        def set_hight(choice):
            if choice == '矮小':
                new_player['身高'] = 160
            elif choice == '普通':
                new_player['身高'] = 170
            elif choice == '高大':
                new_player['身高'] = 180
            set_outline()

        def set_build(choice):
            if choice == '瘦弱':
                new_player['体重'] = 160
            elif choice == '普通':
                new_player['体重'] = 170
            elif choice == '肥胖':
                new_player['体重'] = 180
            set_outline()

        def finish():
            game.pline('-')
            game.clr_cmd(clr_default_flow=False)  # 清除选项
            game.pl('玩家身高为：' + new_player['身高'])
            game.pl('玩家身材为：' + new_player['身材'])
            game.pl('是否接受?')
            ans = lib.yes_or_no()
            if ans == True:
                game.data['人物库'].append(new_player)
                game.clr_screen()  # 清除屏幕
                main_func()
            else:
                set_outline()
        game.clr_cmd(clr_default_flow=False)  # 清除选项
        game.pline()
        game.pl('请设置玩家外形:')
        game.p('身高:')
        lib.list_nums(['矮小', '普通', '高大'], set_hight, new_player['身高'])
        game.pl()
        game.p('身材:')
        lib.list_nums(['瘦弱', '普通', '肥胖'], set_build, new_player['体重'])
        game.pl()
        game.pcmd('确定', 1, finish)

    set_gender()
