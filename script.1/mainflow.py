# -*- coding: UTF-8 -*-
import time
import json
import random
import core.game as game
import script.base_lib as lib
import script.era_lib as era
import script.dlc_lib as dlc
import script.gui_lib as gui
import script.item_lib as ilib
import script.world_lib as world


#<BIOS>#
def open_func(*args):
    game.p('\n'*50)
    game.p('Now Loading...')
    game.def_style('notice', foreground='#DCE441')
    game.def_style('title', foreground='#E1FFFF')
    init()
    # time.sleep(1)
    game.p('OK!')
    time.sleep(1)
    gui.goto(open_menu)


def init():
    if not '人物库' in game.data.keys():
        game.data['人物库'] = []
    game.data['项目库'] = []
    gui.init()
    world.init()
    dlc.init()


def open_menu():
    game.clr_cmd(clr_default_flow=False)
    game.p('\n'*50)
    game.pl('EraLife')
    game.p('\n'*1)
    game.pcmd('[001]  开始游戏', 1)
    game.pl()
    game.pcmd('[002]  读取游戏', 2)
    game.pl()
    game.pcmd('[003]  DLC 管理', 3)

    @game.set_deal_cmd_func_deco
    def deal_func(order):
        if order == 1:
            new_game()
        if order == 2:
            lib.load_func(open_menu, into_main)
        if order == 3:
            gui.goto(dlc.dlc_gui)
#</BIOS>#


def new_game():
    game.clr_cmd(clr_default_flow=False)
    dlc.load()
    ilib.load()
    import script.intro as intro
    gui.goto(intro.角色创建方式界面)


def into_main():
    if game.data['项目库'] == []:
        init()
        dlc.load()
        ilib.load()
    game.set_default_flow(main_func)


def main_func():
    game.set_default_flow(main_func)
    game.clr_cmd(clr_default_flow=False)
    game.pline()
    import script.time_lib as tlib
    game.p(tlib.get_full_time())
    print(game.data['人物库'])
    game.p('  存款：'+str(era.get_player()['状态']['存款']))
    game.pl()
    game.p(era.get_player()['姓名'] + ' '*2)
    game.p('等级：' + str(era.get_player()['等级']))
    game.pl()
    game.p('体力：')
    game.p(lib.value_bar(era.get_player()[
           '状态']['体力'], era.get_player()['属性']['体力上限'], 8))
    # game.pl()
    game.p(' 耐力：')
    game.p(lib.value_bar(era.get_player()[
           '状态']['耐力'], era.get_player()['属性']['耐力上限'], 8))
    # game.pl()
    game.p(' 精力：')
    game.p(lib.value_bar(era.get_player()[
           '状态']['精力'], era.get_player()['属性']['精力上限'], 8))
    game.pline('--')
    # 生成功能列表
    i = 1
    func_list = []
    for each in game.data['项目库']:
        if each['type'] == '功能' and each['parent'] == 'main':
            func_list.append(each)
    for i, each in enumerate(era.sort_list_by_name(func_list)):
        text = '[{:0>3}]'.format(i+1)
        text += each['名称']
        text += ' ' * (6-len(each['名称']))*2
        game.pcmd(text, i+1, gui.goto, each['func'])
        if i % 5 == 4:
            game.pl()
    game.pl()
