# -*- coding: UTF-8 -*-
import random
import core.game as game
import script.base_lib as lib
import script.gui_lib as gui


def get_player():
    for each in game.data['人物库']:
        if each['身份'] == '主角':
            return each


def buy(sb, sth):
    buyer = get_hero_by_hash(sb)
    item = get_item_by_name(sth)
    if buyer['状态']['存款'] >= item['价格']:
        buyer['物品'].append(item['名称'])
        buyer['状态']['存款'] -= item['价格']
        t = '[{}购买了{}，用了${}]'
        t = t.format(buyer['姓名'], item['名称'], item['价格'])
        game.pl(t)
    else:
        game.pl('金钱不足')
    gui.repeat()


def show_shop(item_type):
    l = []
    for each in game.data['项目库']:
        if each['type'] == item_type:
            l.append(each)
    i = 0
    for i, each in enumerate(l):
        if not found_in_sb_pack(get_player(), each['名称']):
            name = each['名称']
            cost = each['价格']
            text = '{}'.format(name)
            text += ' ' * (14-len(name)*2-len(str(cost)))
            text += '${}'.format(cost)
            game.pcmd(text, i+1, buy,
                      (get_player()['hash'], name))
            if i % 5 == 4:
                game.pl()
            else:
                game.p('|')
    game.pline('--')
    text = '[{:0>3}]'.format(i+2)
    text += '返回'
    text += ' ' * (6-len('返回'))*2
    game.pcmd(text, i+2, gui.back)


def get_item_by_name(name):
    for each in game.data['项目库']:
        if each['名称'] == name:
            return each
    return False


def get_hero_by_hash(hash):
    for each in game.data['人物库']:
        if each['hash'] == hash:
            return each
    return False


def get_item(item_type, parent=None):
    l = []
    for each in game.data['项目库']:
        if each['type'] == item_type:
            if parent:
                if parent == each['parent']:
                    l.append(each)
            else:
                l.append(each)
    return l


def sort_list_by_name(l, order_name='main'):
    order_list = []
    for each in get_item('排序'):
        if each['name'] == order_name:
            order_list = each['排序']

    def sort(item):
        for i, each in enumerate(order_list):
            if each == item['name']:
                return i
        return 1+1
    return sorted(l, key=sort)


def give_sb_sth(sb, sth):
    for each in game.data['人物库']:
        if each['hash'] == sb:
            each['物品'].append(sth)


def found_in_sb_pack(sb, sth):
    for each in game.data['人物库']:
        if each['hash'] == sb['hash']:
            print(sth, each['物品'])
            if sth in each['物品']:
                return True
    return False


def get_man_by_hash(hash):
    for each in game.data['人物库']:
        if each['hash'] == hash:
            return each
    return False


def show_profile(hash):
    # 姓名 性别 等级 种族 升级经验条
    game.clr_cmd(clr_default_flow=True)  # 清除选项
    game.pline()
    man = get_man_by_hash(hash)
    game.p('{} '.format(man['姓名']))
    game.p('{} '.format(man['性别']))
    game.p('LV.{} '.format(man['等级']))
    tmp = []
    for each in man['特征']['种族']:
        tmp.append('[{}]'.format(each))
    tmp = ''.join(tmp)
    game.p('{} '.format(tmp))
    game.p('经验值：')
    game.p(lib.value_bar(man['状态']['经验值'], man['属性']['经验值上限'], 15))
    game.pl()
    # 体力 耐力 精力
    game.p('体力：')
    game.p(lib.value_bar(man['状态']['体力'], man['属性']['体力上限'], 8))
    # game.pl()
    game.p(' 耐力：')
    game.p(lib.value_bar(man['状态']['耐力'], man['属性']['耐力上限'], 8))
    # game.pl()
    game.p(' 精力：')
    game.p(lib.value_bar(man['状态']['精力'], man['属性']['精力上限'], 8))
    # 体型 身高 胸围 腰围 臀围
    game.pl('◯ 体型：{} ◯ '.format(man['特征']['体型']) + '-'*20)
    game.p('身高：{}cm '.format(man['属性']['身高']))
    game.p('体重：{}kg '.format(man['属性']['体重']))
    game.p('胸围：{}cm '.format(man['属性']['胸围']))
    game.p('腰围：{}cm '.format(man['属性']['腰围']))
    game.p('臀围：{}cm '.format(man['属性']['臀围']))
    # 特征（突出的外形、性格、属性特征）
    game.pl('◯ 特征 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    # 宝珠（）
    game.pl('◯ 宝珠 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    # 经验
    game.pl('◯ 经验 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    # 异常经验
    game.pl('◯ 异常经验 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    # 能力
    game.pl('◯ 能力 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    # 刻印
    game.pl('◯ 刻印 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    #     性格
    game.pl('◯ 性格 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    #     体质
    game.pl('◯ 体质 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    #     技能
    game.pl('◯ 技能 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    #     魔法
    game.pl('◯ 魔法 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    #     后天
    game.pl('◯ 后天 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    #     弱点
    game.pl('◯ 弱点 ◯ ' + '-'*20)
    for each in man['特征']:
        game.p('[{}]'.format(each))
    # 物品
    game.pl('◯ 物品 ◯ ' + '-'*20)
    for each in man['物品']:
        game.p('[{}]'.format(each))
    game.pline('--')
    # --------------------------------
    # 助手职责
    # 外观指示
    # 服装
    # 社会关系
    # 相性
    # 作品
    # 榨精排行
    game.pcmd('返回', 1, gui.back)
