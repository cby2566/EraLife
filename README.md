# EraLife

欢迎来到 EraLife 项目！

本项目是基于 [qsjl11](https://github.com/qsjl11) 的 [pyera](https://github.com/qsjl11/pyera) 所开发的一款新的Era类游戏。

并且根据传统：:underage: 

**公告：由于中国大陆地区尚未实现内容分级等类似措施，为积极响应当地法律法规，本游戏将不会面向中国大陆地区发布，也谢绝任何人将该游戏在中国大陆地区以任何方式进行传播，若违反该规定，造成的任何后果作者概不负责。特此声明！**


