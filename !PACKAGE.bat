@echo off
python setup.py build
xcopy erajs "build/exe.win-amd64-3.6/erajs" /e /i /y
xcopy config "build/exe.win-amd64-3.6/config" /e /i /y
xcopy data "build/exe.win-amd64-3.6/data" /e /i /y
xcopy dlc "build/exe.win-amd64-3.6/dlc" /e /i /y
xcopy mod "build/exe.win-amd64-3.6/mod" /e /i /y
xcopy script "build/exe.win-amd64-3.6/script" /e /i /y
xcopy wiki "build/exe.win-amd64-3.6/wiki" /e /i /y
pause